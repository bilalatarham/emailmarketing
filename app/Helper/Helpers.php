<?php

function checkImage($path = '', $placeholder = '')
{
    if (empty($placeholder))
    {
        $placeholder = 'placeholder.png';
    }

    if (!empty($path))
    {
        $url = explode('storage', $path);
        $url = public_path() . '/storage' . $url[1];
        $isFile = explode('.', $url);
        if (file_exists($url) && count($isFile) > 1)
            return $path;
        else
            return asset('images/' . $placeholder);
    }
    else
    {
        return asset('images/' . $placeholder);
    }
}

function sendEmail($email, $subject, $content, $pdf = '', $filename = '',$lang = 'en')
{
    $lang_file = public_path('i18n/translations/'.$lang.'.json');
    $lang_arr = json_decode(file_get_contents($lang_file),true);
    
    $asset = asset('/');

    try {
        \Mail::send('emails.template', ['content' => $content ,'lang_arr' => $lang_arr,'asset_url' => $asset] , function ($message) use ($email, $subject, $pdf, $filename)
        {
            $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $message->to($email);
            $message->subject($subject);

            if (!empty($pdf))
                $message->attachData($pdf->output(), $filename);
        });
    } catch (\Exception $e) {
        \Log::info('Send Email Exception', array(
            'Message' => $e->getMessage()
        ));
    }
}

function rights()
{
    $result = DB::table('rights')
            ->select('rights.id', 'rights.name as right_name', 'modules.name as module_name')
            ->join('modules', 'rights.module_id', '=', 'modules.id')
            ->where(['rights.status' => 1])
            ->get()
            ->toArray();

    $array = [];

    for ($i = 0; $i < count($result); $i++)
    {
        $array[$result[$i]->module_name][] = $result[$i];
    }
    return $array;
}

function have_right($right_id)
{
    $user = \Auth::user();
    if ($user['role_id'] == 1)
    {
        return true;
    }
    else
    {
        $result = \DB::table('roles')
                ->where('id', $user['role_id'])
                ->whereRaw("find_in_set($right_id,right_ids)")
                ->first();

        if (!empty($result))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

function access_denied()
{
    abort(403, 'You have no right to perform this action.');
}

function settingValue($key)
{
    $setting = \DB::table('settings')->where('option_name', $key)->first();
    if ($setting)
        return $setting->option_value;
    else
        return '';
}

function getRecord($tbl, $where)
{
    $record = \DB::table($tbl)->where($where)->first();
    if ($record)
    {
        return $record;
    }
    else
    {
        return "";
    }
}

function getValue($tbl, $column, $where)
{
    $record = \DB::table($tbl)->where($where)->first();
    if ($record)
    {
        return $record->$column;
    }
    else
    {
        return "";
    }
}

function createNotification($type, $user_id, $message, $link, $fa_class)
{
    $notifications = [
        'type' => $type,
        'user_id' => $user_id,
        'message' => $message,
        'link' => $link,
        'fa_class' => $fa_class,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
    ];

    $id = DB::table('notifications')->insertGetId($notifications);

    $notification = \App\Models\Notification::find($id);
    broadcast(new \App\Events\NotificationSent($notification))->toOthers();
    return $notification;
}

function getNotifications($user_id, $is_read, $type) //$type 1 for admin and 2 for user
{
    if ($is_read == -1) // all
        $conditions = ['type' => $type];
    else
        $conditions = ['type' => $type, 'is_read' => $is_read];

    if ($type == 2) // user
    {
        $conditions['user_id'] = $user_id;
    }

    return \App\Models\Notification::select('*')
                    ->where($conditions)
                    ->orderBy('created_at', 'DESC')
                    ->get();
}

function activatePackage($user_id, $package)
{
    $packageLinkedFeatures = $package->linkedFeatures->pluck('count','feature_id')->toArray();

    $packageSubscription = \App\Models\PackageSubscription::create([
                'user_id'               =>  $user_id,
                'package_id'            =>  $package->id,
                'package_setting_id'    =>  2, // Credit user with the 1st package settings i.e price etc for migration
                'per_migration_price'   =>  0,
                'price'                 =>  0,
                // 'features'              =>  empty($package->linkedFeatures) ? '' : json_encode($packageLinkedFeatures),
                'description'           =>  $package->description,
                'start_date'            =>  \Carbon\Carbon::now('UTC')->timestamp,
                'end_date'              =>  Null,
                'is_active'             =>  1
                // 'user_id' => $user_id,
                // 'package_id' => $package->id,
                // 'per_migration_price'   =>  $package->per_unit_price,
                // 'price' => $package->total_price,
                // 'features' => empty($package->linkedFeatures) ? '' : json_encode($packageLinkedFeatures),
                // 'description' =>  $package->description,
                // 'start_date' => \Carbon\Carbon::now('UTC')->timestamp,
                // 'end_date' => Null,
                // 'is_active' => 1
    ]);

    \App\User::where('id', $user_id)->update([
        'package_id' => $package->id,
        'package_subscription_id' => $packageSubscription->id,
        'on_trial' => 0
    ]);

    if(array_key_exists(1, $packageLinkedFeatures))
    {
        \App\User::where('id', $user_id)->update([
            'total_migrations' => $packageLinkedFeatures[1],
            'remaining_migrations' => $packageLinkedFeatures[1],
            'queue_migration' => 0,
            'switch_to_paid_package' => 0
        ]);
    }
}

function userPackageExpiration($user_id, $status)
{
    \App\User::where('id', $user_id)->update([
        'is_expired' => $status
    ]);
}

function durationConversion($seconds)
{
    $time = gmdate("H:i:s", $seconds);
    $timeArr = explode(':', $time);
    $durationStr = '';

    if ($timeArr[0] != '00')
        $durationStr .= $timeArr[0] . ' hr ';
    if ($timeArr[1] != '00')
        $durationStr .= $timeArr[1] . ' min ';
    if ($timeArr[2] != '00')
        $durationStr .= $timeArr[2] . ' sec';

    return $durationStr;
}

function getPreviousMonthDates($from,$to,$timezone)
{
    $to_date = new \Carbon\Carbon($to);
    $from_date = new \Carbon\Carbon($from);

    $diff = $to_date->diffInDays($from_date);

    $prev_to = \Carbon\Carbon::createFromTimeStamp($from,"UTC")->tz($timezone)->subDay()->toDateString();
    $prev_to = $prev_to.' 23:59:59';

    $prev_from = \Carbon\Carbon::createFromTimeStamp(strtotime($prev_to), "UTC")->tz($timezone)->subDays($diff+1)->toDateString();
    $prev_from = $prev_from.' 00:00:00';

    return array(
        'from' => $prev_from,
        'to'   => $prev_to,
    );
}

function formatBytes($size, $precision = 2)
{
    if ($size > 0) {
        $size = (int) $size;
        $base = log($size) / log(1024);
        $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    } else {
        return $size;
    }
}

function convertBytesToGigaBytes($bytes)
{
    return number_format($bytes/1073741824,2);
}

function convertToByte($p_sFormatted) {
    $aUnits = array('B'=>0, 'KB'=>1, 'MB'=>2, 'GB'=>3, 'TB'=>4, 'PB'=>5, 'EB'=>6, 'ZB'=>7, 'YB'=>8);
    $sUnit = strtoupper(trim(substr($p_sFormatted, -2)));
    if (intval($sUnit) !== 0) {
        $sUnit = 'B';
    }
    if (!in_array($sUnit, array_keys($aUnits))) {
        return false;
    }
    $iUnits = trim(substr($p_sFormatted, 0, strlen($p_sFormatted) - 2));
    if (!intval($iUnits) == $iUnits) {
        return false;
    }
    return $iUnits * pow(1024, $aUnits[$sUnit]);
}

function getCloudMetaData($cloud_id)
{
    $largeImage = '';
    $smallImage = '';
    $name = '';
    $slug = '';

    switch ($cloud_id) {
        case config('constants.cloud_platforms')['GOOGLE_DRIVE']:
            $largeImage = '/images/googledrive.png';
            $smallImage = '/images/small-googledrive.png';
            $name = 'Google Drive';
            $slug = 'google-drive';
            break;
        case config('constants.cloud_platforms')['DROPBOX']:
            $largeImage = '/images/dropbox.png';
            $smallImage = '/images/small-dropbox.png';
            $name = 'Dropbox';
            $slug = 'dropbox';
            break;
        case config('constants.cloud_platforms')['ONE_DRIVE']:
            $largeImage = '/images/onedrive1.png';
            $smallImage = '/images/small-onedrive1.png';
            $name = 'One Drive';
            $slug = 'one-drive';
            break;
        case config('constants.cloud_platforms')['IONOS_HI_DRIVE']:
            $largeImage = '/images/ionoshidrive.png';
            $smallImage = '/images/small-ionoshidrive.png';
            $name = 'IONOS Hidrive';
            $slug = 'ionos-hidrive';
            break;
        case config('constants.cloud_platforms')['AIKQ']:
            $largeImage = '/images/aik-100x100.png';
            $smallImage = '/images/aik-icon20x20.png';
            $name = 'AIKQ';
            $slug = 'aikq-webdisk';
            break;
    }

    return array(
        'largeImage' => $largeImage,
        'smallImage' => $smallImage,
        'name' => $name,
        'slug' => $slug
    );
}

function translation($item_id,$language_module_id,$lang,$column_name,$org_value)
{
    $record = \App\Models\LanguageTranslation::where(['item_id' => $item_id, 'language_module_id' => $language_module_id, 'language_code' => $lang, 'column_name' => $column_name])->first();

    if(!empty($record))
        return $record->item_value;
    else
        return $org_value;
}

function translationByDeepL($text,$target_lang)
{
    if(empty($text))
        return $text;
    else
    {
        if($target_lang == 'br')
        {
            $target_lang = 'pt-BR';
        }

        $params = array(
            'auth_key' => 'd554170c-80ad-7185-f19c-b776394eb975',
            'text' => $text,
            'target_lang' => $target_lang
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.deepl.com/v2/translate?" . http_build_query($params),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $responseArr = json_decode($response, true);

        if(array_key_exists('translations', $responseArr))
        {
            return $responseArr['translations'][0]['text'];
        }
        else
        {
            return $text;
        }
    }
}

function transformEmailTemplateModel($model,$lang)
{
    //$subject = $model->subject;
    $subject = translation($model->id,4,$lang,'subject',$model->subject);
    $content = $model->content;

    $search = [];
    $replace = [];
    $ids = [];
    $labels = $model->emailTemplateLabels;

    foreach($labels as $object)
    {
        $search[$object->id] = '{{'.$object->label.'}}';
        $replace[$object->id] = $object->value;
        $ids[] = $object->id;
    }
    
    if($lang != 'en')
    {
        $translations = \App\Models\LanguageTranslation::where(['language_module_id' => 10, 'language_code' => $lang, 'column_name' => 'value'])->whereIn('item_id',$ids)->get();

        foreach($translations as $translation)
        {
            $replace[$translation->item_id] = $translation->item_value;
        }
    }

    $content  = str_replace($search,$replace,$content);

    return [
        'id'      => $model->id,
        'subject' => $subject,
        'content' => $content,
        'lang'    => $lang,
        'type'    => $model->type,
        'info'    => $model->info,
        'status'  => $model->status
    ];
}

function createAndScanFile($user_id,$file_name,$content)
{
    $upload_path = public_path().'/cloudscanning/users/'.$user_id.'/cloud-files';
    $file_name = str_replace("/","",$file_name);
    $temp_file_name = uniqid().'-'.$file_name;
    
    $fileName = $upload_path.'/'.$temp_file_name;
    if(!\File::isDirectory($upload_path))
    {
        \File::makeDirectory($upload_path, 0777, true, true);
        \File::put($fileName,$content);
    }
    else
    {
        \File::put($fileName,$content);
    }

    /*
    * Start scan file
    */

    $found_virus = false;
    
    if(\App::environment() === 'production')
    {
        $output = shell_exec('scan -a -f '.$fileName.'');
        if(strpos($output,'OK') || $output == null) {
            $found_virus = false;
        } else {
            $found_virus = true;
        }
    }

    /*
    * End scan file
    */
    if(file_exists($fileName))
    {
        \File::delete($fileName);
    }

    return $found_virus;
}

?>