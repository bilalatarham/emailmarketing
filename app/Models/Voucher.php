<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    protected $fillable = [
        'user_id',
        'voucher',
        'reseller',
        'expiry_time',
        'data',
        'status'
    ];
}