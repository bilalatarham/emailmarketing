<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CloudMigrationReportFile extends Model
{
    protected $fillable = [
        'user_id', 'report_id', 'name', 'type', 'size', 'processed_on', 'timestamp', 'status'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function report()
    {
        return $this->belongsTo(CloudMigrationReport::class, 'report_id');
    }
}