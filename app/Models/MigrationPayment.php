<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MigrationPayment extends Model
{
    protected $fillable = [
        'user_id', 'package_subscription_id', 'no_of_migrations', 'total_amount', 'txn_id', 'mollie_customer_id', 'data', 'lang', 'status', 'payload','txn_id', 'mollie_customer_id', 'data', 'lang', 'status'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

    public function subscription()
    {
      return $this->belongsTo(PackageSubscription::class, 'package_subscription_id');
    }
}