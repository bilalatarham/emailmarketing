<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Encryption\DecryptException;

class AccountSetting extends Model
{
    protected $fillable = [
        'user_id', 'address', 'card_holder_name', 'card_brand', 'card_number', 'card_last_four_digits', 'expire_month', 'expire_year', 'cvc', 'migration_init_notification', 'migration_complete_notification','blacklist_email'
    ];

    public function getCardNumberAttribute($value)
    {
        try {
            return decrypt($value);
        } catch (DecryptException $e) {
            return $value;
        }
    }

    public function getCvcAttribute($value)
    {
        try {
            return decrypt($value);
        } catch (DecryptException $e) {
            return $value;
        }
    }

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }
}