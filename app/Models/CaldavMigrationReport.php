<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CaldavMigrationReport extends Model
{
    //
    protected $fillable = [
        'user_id', 'source_server', 'source_email', 'destination_server', 'destination_email', 'no_of_processed_calendar', 'no_of_processed_contacts', 'migrate_option', 'notify_emails', 'status', 'processed_on','package_subscription_id',
        'package_payload','voucher_id', 'is_prior_to_pay_as_you_go'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }
}
