<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CloudMigrationReport extends Model
{
    protected $fillable = [
        'user_id', 'source_cloud_id', 'source_selected_path_ids', 'destination_cloud_id', 'destination_selected_directory_id', 'file_name', 'no_of_total_files_folders', 'no_of_processed_files_folders', 'no_of_conflict_files_folders', 'notify_me', 'notify_emails', 'processed_on','timestamp', 'status','package_subscription_id' , 'package_payload','cost_payload','voucher_id' , 'is_prior_to_pay_as_you_go'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function sourceCloud()
    {
        return $this->belongsTo(Cloud::class, 'source_cloud_id');
    }

    public function destinationCloud()
    {
        return $this->belongsTo(Cloud::class, 'destination_cloud_id');
    }

    public function files()
    {
        return $this->hasMany(CloudMigrationReportFile::class, 'report_id');
    }
}