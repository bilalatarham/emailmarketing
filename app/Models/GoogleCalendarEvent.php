<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleCalendarEvent extends Model
{
    protected $fillable = [
      'schedule_caldav_migration_id', 'google_calendar_id', 'payload'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function scheduleCaldavMigration()
    {
      return $this->belongsTo(ScheduleCaldavMigration::class, 'schedule_caldav_migration_id');
    }

    public function googleCalendar()
    {
      return $this->belongsTo(GoogleCalendar::class, 'google_calendar_id');
    }
}
