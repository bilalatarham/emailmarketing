<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cloud extends Model
{
    protected $fillable = [
        'user_id', 'cloud_id', 'username', 'password', 'account_id', 'name', 'email', 'access_token', 'refresh_token', 'token_type', 'expires_in', 'timestamp', 'total_space', 'used_space', 'available_space', 'auth_response', 'status'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}