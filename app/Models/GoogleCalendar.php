<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleCalendar extends Model
{
    protected $fillable = [
      'schedule_caldav_migration_id', 'payload'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function scheduleCaldavMigration()
    {
      return $this->belongsTo(ScheduleCaldavMigration::class, 'schedule_caldav_migration_id');
    }

    public function googleCalendarEvent()
    {
      return $this->hasMany(GoogleCalendarEvent::class, 'google_calendar_id');
    }
}
