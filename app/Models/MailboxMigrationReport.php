<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailboxMigrationReport extends Model
{
    protected $fillable = [
        'user_id', 'source_server_type', 'source_server_name', 'source_server_url', 'source_email_address', 'source_password', 'destination_server_type', 'destination_server_name', 'destination_server_url', 'destination_email_address', 'destination_password', 'output', 'migrate_option' ,'initiated_at' ,'notify_email', 'processed_on','timestamp', 'status','package_subscription_id','package_payload','cost_payload','voucher_id', 'is_prior_to_pay_as_you_go'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}