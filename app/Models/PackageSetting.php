<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageSetting extends Model
{
    protected $fillable = [
        'name',
        'info',
        'start_migration_range',
        'end_migration_range',
        'mailbox_migration_data_volume',
        'cloud_migration_data_volume',
        'price_without_vat',
        'price_with_vat',
        'is_caldav_cardav_migration',
        'is_voucher_setting',
        'status'
    ];
}