<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleCaldavMigration extends Model
{
    protected $fillable = [
      'user_id', 'caldav_migration_report_id', 'processed_on'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

    public function caldavMigrationReport()
    {
      return $this->belongsTo(CaldavMigrationReport::class, 'caldav_migration_report_id');
    }

    public function googleCalendars()
    {
      return $this->hasMany(GoogleCalendar::class, 'schedule_caldav_migration_id');
    }
}
