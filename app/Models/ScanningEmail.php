<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScanningEmail extends Model
{
    protected $fillable = ['user_id','folder_name', 'email_subject','scan_content_status','scan_attachment_status','status','migration_id','message_uid'];
}
