<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable = [
        'name', 'description', 'image', 'hovered_image', 'status'
    ];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($model) 
        {
            $path = 'storage/features/'.$model->image;
            $hovered_img_path = 'storage/features/'.$model->hovered_image;
            if (\File::exists(public_path() . '/' . $path)) 
            {
                \File::delete($path);
            }
            if (\File::exists(public_path() . '/' . $hovered_img_path)) 
            {
                \File::delete($path);
            }
        });
    }

    // ************************** //
    //  Append Extra Attributes   //
    // ************************** //

    protected $appends = ['image_path', 'hovered_image_path'];

    public function getImagePathAttribute()
    {
        return $this->attributes['image_path'] = checkImage(asset('storage/features/' . $this->image),'placeholder.png');
    }

    public function getHoveredImagePathAttribute()
    {
        return $this->attributes['hovered_image_path'] = checkImage(asset('storage/features/' . $this->hovered_image),'placeholder.png');
    }
}
