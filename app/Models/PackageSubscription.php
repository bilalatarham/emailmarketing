<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;

class PackageSubscription extends Model
{
    protected $fillable = [
        'user_id', 'package_id', 'package_setting_id', 'total_used_migrations', 'payment_queued_migrations', 'per_migration_price', 'price', 'extra_data_price', 'discounted_amount', 'vat_amount', 'total_migration_price', 'features', 'description', 'start_date', 'end_date', 'is_active', 'is_prior_to_pay_as_you_go'
    ];

    // ************************** //
    //        Relationships       //
    // ************************** //

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

    public function package()
    {
      return $this->belongsTo(Package::class, 'package_id');
    }

    public function packageSetting()
    {
      return $this->belongsTo(PackageSetting::class, 'package_setting_id');
    }


    public function payment()
    {
      return $this->hasOne(Payment::class, 'subscription_id');
    }

    public function payments()
    {
        return $this->hasMany(MigrationPayment::class, 'package_subscription_id');
    }

    public function pendingPayment()
    {
        return $this->hasOne(MigrationPayment::class, 'package_subscription_id')->where('status','!=',1);
    }

    // ************************** //
    //        	Attributes        //
    // ************************** //

    public function getPackageImageAttribute()
    {
      return $this->attributes['package_image'] = checkImage(asset('storage/packages/' . $this->package->icon),'placeholder.png');
    }

    public function getPackageTitleAttribute()
    {
      return $this->attributes['package_title'] = $this->package->title;
    }

    public function getLinkedFeaturesAttribute()
    {
      $features = json_decode($this->features,true);
      $linked_features = [];

      if(!empty($features))
      {
        foreach($features as $key => $value)
        {
            $packageFeature = PackageFeature::find($key);

            $arr['id']   = $key;
            $arr['name'] = $packageFeature->name;
            $arr['info'] = $packageFeature->info;
            $arr['count']= $value;

            $linked_features[] = $arr;
        }
      }

    	return $this->attributes['linked_features'] = $linked_features;
    }
}