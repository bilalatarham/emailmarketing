<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Validation\Rule;
use Session;
use Hashids;
use Auth;
use Storage;
use DataTables;
use Carbon\Carbon;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!have_right(27))
        access_denied();

        $data = [];
        if ($request->ajax())
        {
            $db_record = User::select(
                            'users.*',
                            'package_subscriptions.*')
                        ->join('package_subscriptions', function ($join) {
                            $join->on('package_subscriptions.id', '=', 'users.package_subscription_id');
                        })
                        ->orderBy('package_subscriptions.created_at','DESC');
                        //->get();

            $datatable = Datatables::of($db_record);
            $datatable = $datatable->addIndexColumn();
            $datatable = $datatable->editColumn('price', function($row)
            {
                return '<sup>'.config('constants.currency')['symbol'].'</sup>'.$row->price;
            });
            $datatable = $datatable->addColumn('package_title', function($row)
            {
                return $row->subscription->package_title;
            });
            $datatable = $datatable->editColumn('last_login', function($row)
            {
                return (!empty($row->last_login)) ? Carbon::createFromTimeStamp($row->last_login, "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') : 'N/A' ;
            });
            $datatable = $datatable->editColumn('start_date', function($row)
            {
                return Carbon::createFromTimeStamp($row->start_date, "UTC")->tz(session('timezone'))->format('d M, Y') ;
            });
            $datatable = $datatable->editColumn('end_date', function($row)
            {
                return (!empty($row->end_date)) ? Carbon::createFromTimeStamp($row->end_date, "UTC")->tz(session('timezone'))->format('d M, Y') : 'Lifetime';
            });
            $datatable = $datatable->editColumn('status', function($row)
            {
                $currentTimestamp = Carbon::now('UTC')->timestamp;
                $status = '';
                
                if(empty($row->subscription->end_date) || $row->subscription->end_date > $currentTimestamp)
                    $status = '<span class="label label-success">Active</span>';
                else
                    $status = '<span class="label label-warning">Expired</span>';

                return $status;
            });
            
            $datatable = $datatable->rawColumns(['price','status']);
            $datatable = $datatable->make(true);
            return $datatable;
        }

        return view('admin.subscriptions.index',$data);
    }
}
