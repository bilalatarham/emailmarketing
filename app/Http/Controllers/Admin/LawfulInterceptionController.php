<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\MailboxMigrationController;
use App\User;
use App\Models\CaldavMigrationReport;
use App\Models\CloudMigrationReport;
use App\Models\MailboxMigrationReport;
use App\Models\Payment;
use App\Models\PackageSubscription;
use App\Classes\ExtendedZip;
use App\Jobs\ArchiveUsersDataJob;
use DataTables;
use Session;
use DB;
use File;
use Storage;
use Hashids;
use LaravelPDF;
use Carbon\Carbon;
use ZipArchive;

class LawfulInterceptionController extends Controller
{
    /**
     * Create a new LawfulInterceptionController instance.
     *
     * @return void
     */
    public function __construct()
    {
        session_start();
    }

    public function index(Request $request)
    {
        if(!have_right(86))
            access_denied();

        $data = [];

        if($request->ajax())
        {

            $post = $request;
            
            $db_record = new User();

            if($request->has('search') && !empty($request->search))
            {
                $db_record = $db_record->where(function($q) use ($request) {
                    $q->where('name', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('username', 'LIKE', '%' . $request->search . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->search . '%');
                });
            }
            else
            {
                $db_record =  $db_record->whereNull('id');
            }

            $datatable = Datatables::of($db_record);
            $datatable = $datatable->addIndexColumn();

            $datatable = $datatable->editColumn('status', function($row)
            {
                $status = '<span class="label label-danger">Disable</span>';
                if ($row->status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                }
                else if ($row->status == 2)
                {
                    $status = '<span class="label label-warning">Unverified</span>';
                } 
                else if ($row->status == 3)
                {
                    $status = '<span class="label label-danger">Deleted</span>';
                }

                return $status;
            });
            $datatable = $datatable->addColumn('action', function($row) use ($post)
            {
                $actions = '<span class="actions">';

                if(have_right(89))
                {
                    $actions .= '&nbsp;<a title="User Details PDF" class="btn btn-primary" target="_blank" href="'.url("admin/lawful-interception/user-details-pdf/" . Hashids::encode($row->id)).'"><i class="fa fa-user"></i></a>';
                }

                if(have_right(90))
                {
                    // $actions .= '&nbsp;<a title="Contacts / Calendar Migration Reports" class="btn btn-primary lawful-user-files-download" data-id="'.Hashids::encode($row->id).'" data-from="'.$post->from.'" data-to="'.$post->to.'" href="#"><i class="fa fa-calendar"></i></a>';

                    $actions .= '&nbsp;<a title="Contacts / Calendar Migration Reports" class="btn btn-primary lawful-user-files-download" href="'.url("admin/lawful-interception/caldav-cardav-migration-reports/" . Hashids::encode($row->id)).'?from='.$post->from.'&to='.$post->to.'"><i class="fa fa-calendar"></i></a>';
                }

                if(have_right(91))
                {
                    $actions .= '&nbsp;<a title="Cloud Migration Reports" class="btn btn-primary lawful-user-files-download" href="'.url("admin/lawful-interception/cloud-migration-reports/" . Hashids::encode($row->id)).'?from='.$post->from.'&to='.$post->to.'"><i class="fa fa-cloud"></i></a>';
                }

                if(have_right(92))
                {
                    $actions .= '&nbsp;<a title="Mailbox Migration Reports" class="btn btn-primary lawful-user-files-download" href="'.url("admin/lawful-interception/mailbox-migration-reports/" . Hashids::encode($row->id)).'?from='.$post->from.'&to='.$post->to.'"><i class="fa fa-envelope"></i></a>';
                }

                if(have_right(93))
                {
                    $actions .= '&nbsp;<a title="User Payments PDF" class="btn btn-primary" target="_blank" href="'.url("admin/lawful-interception/user-payments-pdf/" . Hashids::encode($row->id)).'?from='.$post->from.'&to='.$post->to.'"><i class="fa fa-credit-card-alt"></i></a>';
                }

                if(have_right(94))
                {
                    $actions .= '&nbsp;<a title="User Subscriptions PDF" class="btn btn-primary" target="_blank" href="'.url("admin/lawful-interception/user-subscriptions-pdf/" . Hashids::encode($row->id)).'?from='.$post->from.'&to='.$post->to.'"><i class="fa fa-list"></i></a>';
                }

                if(have_right(95))
                {
                    $actions .= '&nbsp;<a title="Download All Data" class="btn btn-primary lawful-download-user-data" data-id="'.Hashids::encode($row->id).'" data-from="'.$post->from.'" data-to="'.$post->to.'" href="#"><i class="fa fa-download"></i></a>';
                    // $actions .= '&nbsp;<a title="Download All Data" class="btn btn-primary lawful-user-files-download" href="'.url("admin/lawful-interception/download-all-data/" . Hashids::encode($row->id)).'?from='.$post->from.'&to='.$post->to.'"><i class="fa fa-download"></i></a>';
                }

                $actions .= '</span>';
                return $actions;
            });

            $datatable = $datatable->rawColumns(['status','action']);
            $datatable = $datatable->make(true);
            return $datatable;
        }

        return view('admin.lawful-interception.index')->with($data);
    }

    public function userDetailsPdf($id)
    {
        if(!have_right(89))
            access_denied();

        $id = Hashids::decode($id)[0];
        $user = User::find($id);
        $data['user'] = $user;

        $pdf = LaravelPDF::loadView('admin.lawful-interception.user_details_pdf', $data);
        return $pdf->stream($user->email.'-details.pdf');
        // return $pdf->download($user->email.'-user-details.pdf');
    }

    public function archiveCaldavCardavMigrationReports($id, Request $request)
    {
        if(!have_right(90))
            access_denied();

        $lang = !empty(session('lang')) && session('lang') != 'en' ? session('lang') : 'en';
        $lang_file = public_path('i18n/translations/'.$lang.'.json');
        $lang_arr = json_decode(file_get_contents($lang_file),true);

        $data['lang_arr'] = $lang_arr;  
        $data['global_font_family'] = in_array($lang, ['ru','ja','zh']) ? 'chinesefont' : 'arial';

        $from = strtotime($request->from.' 00:00:00');
        $to = strtotime($request->to.' 23:59:59');

        $id = Hashids::decode($id)[0];
        $user = User::find($id);

        $userfilename = $user->temp_zip_file;
        if(isset($userfilename))
        {
            $tmp_file = public_path() . '/storage/temp/' . $userfilename;
            if(file_exists($tmp_file)) 
            {
                unlink($tmp_file);
            }

            $user->update([
                'temp_zip_file' => null
            ]);
        }

        # delete directory
        if (File::exists(public_path() . '/storage/temp/user-contacts-calendar-migration-reports'))
        {
            File::deleteDirectory(public_path() . '/storage/temp/user-contacts-calendar-migration-reports');
        }

        $data['reports'] = CaldavMigrationReport::where('user_id',$id)->where('is_prior_to_pay_as_you_go', 2)->whereNotNull('processed_on')->whereBetween('processed_on', [$from, $to])->orderBy('processed_on','DESC')->get();

        if(count($data['reports']) < 1 )
        {
            Session::flash('flash_danger', 'No record found for this user.');
            return response()->json([
                'url' =>url('/admin/lawful-interception'),
                'status' => 2,
                'message' => 'Data Not Found.'
            ], 200, ['Content-Type' => 'application/json']);
        }

        //MAKE DIRECTORY
        $upload_path = 'public/temp/user-contacts-calendar-migration-reports';
        if (!File::exists(public_path() . '/storage/temp/user-contacts-calendar-migration-reports')) 
        {
            Storage::makeDirectory($upload_path);
        }

        # loop through each file
        foreach ($data['reports'] as $key=> $data['report']) {
            try {
                $pdf = LaravelPDF::loadView('admin.lawful-interception.caldav_cardav_migration_report', $data);    
                $reportFilename = $user->email.'-contacts-calendar-migration-report-'.($key+1).'.pdf';

                // # download file
                Storage::put('public/temp/user-contacts-calendar-migration-reports/'.$reportFilename, $pdf->output());
            } catch (\Exception $e) {

            }
        }

        # create new zip object
        $zip = new ZipArchive();

        # create a temp file & open it
        $tmp_file = tempnam(public_path().'/storage/temp/', '');
        $zip->open($tmp_file, ZipArchive::CREATE);

        # get file from storage
        $files = File::files(public_path() . '/storage/temp/user-contacts-calendar-migration-reports');

        # loop through each file
        foreach ($files as $key => $value) {
            try {
                $relativeNameInZipFile = basename($value);

                #add it to the zip
                $zip->addFile($value, $relativeNameInZipFile);
            } catch (\Exception $e) {

            }
        }

        # close zip
        $zip->close();

        # getting name of temp file
        $filename = basename($tmp_file);

        # Save temp file name in db
        $user->update([
            'temp_zip_file' => $filename
        ]);

        return response()->json([
            'status' => 1,
            'message' => 'Archived caldav migration reports.'
        ], 200, ['Content-Type' => 'application/json']);
    }

    public function caldavMigrationReports($id, Request $request)
    {
        if(!have_right(90))
            access_denied();

        $lang = !empty(session('lang')) && session('lang') != 'en' ? session('lang') : 'en';
        $lang_file = public_path('i18n/translations/'.$lang.'.json');
        $lang_arr = json_decode(file_get_contents($lang_file),true);

        $data['lang_arr'] = $lang_arr;  
        $data['global_font_family'] = in_array($lang, ['ru','ja','zh']) ? 'chinesefont' : 'arial';

        $from = strtotime($request->from.' 00:00:00');
        $to = strtotime($request->to.' 23:59:59');

        $id = Hashids::decode($id)[0];
        $user = User::find($id);
        $data['reports'] = CaldavMigrationReport::where('user_id',$id)->where('is_prior_to_pay_as_you_go', 2)->whereNotNull('processed_on')->whereBetween('processed_on', [$from, $to])->orderBy('processed_on','DESC')->get();

        if(count($data['reports']) < 1 )
        {
            Session::flash('flash_danger', 'No record found for this user.');
            return redirect()->back(); 
        }

        //MAKE DIRECTORY
        $upload_path = 'public/temp/user-contacts-calendar-migration-reports';
        if (!File::exists(public_path() . '/storage/temp/user-contacts-calendar-migration-reports')) 
        {
            Storage::makeDirectory($upload_path);
        }

        # loop through each file
        foreach ($data['reports'] as $key=> $data['report']) {
            try {
                $pdf = LaravelPDF::loadView('admin.lawful-interception.caldav_cardav_migration_report', $data);    
                $reportFilename = $user->email.'-contacts-calendar-migration-report-'.($key+1).'.pdf';

                // # download file
                Storage::put('public/temp/user-contacts-calendar-migration-reports/'.$reportFilename, $pdf->output());
            } catch (\Exception $e) {

            }
        }

        # create new zip object
        $zip = new ZipArchive();

        # create a temp file & open it
        $tmp_file = tempnam(public_path().'/storage/temp/', '');
        $zip->open($tmp_file, ZipArchive::CREATE);

        # get file from storage
        $files = File::files(public_path() . '/storage/temp/user-contacts-calendar-migration-reports');

        # loop through each file
        foreach ($files as $key => $value) {
            try {
                $relativeNameInZipFile = basename($value);

                #add it to the zip
                $zip->addFile($value, $relativeNameInZipFile);
            } catch (\Exception $e) {

            }
        }

        # close zip
        $zip->close();

        $_SESSION['LawfulUserDownloadFiles'] = 1;

        # delete directory
        if (File::exists(public_path() . '/storage/temp/user-contacts-calendar-migration-reports'))
        {
            File::deleteDirectory(public_path() . '/storage/temp/user-contacts-calendar-migration-reports');
        }

        $filename = $user->email.'-contacts-calendar-migration-reports-'.date('Y-m-d').'.zip';

        # send the file to the browser as a download
        header('Content-disposition: attachment; filename="'.$filename.'"');
        header('Content-type: application/zip');
        readfile($tmp_file);
        unlink($tmp_file);
    }

    public function cloudMigrationReports($id, Request $request)
    {
        if(!have_right(91))
            access_denied();

        $lang = !empty(session('lang')) && session('lang') != 'en' ? session('lang') : 'en';
        $lang_file = public_path('i18n/translations/'.$lang.'.json');
        $lang_arr = json_decode(file_get_contents($lang_file),true);

        $data['lang_arr'] = $lang_arr;  
        $data['global_font_family'] = in_array($lang, ['ru','ja','zh']) ? 'chinesefont' : 'arial';

        $from = strtotime($request->from.' 00:00:00');
        $to = strtotime($request->to.' 23:59:59');

        $id = Hashids::decode($id)[0];
        $user = User::find($id);
        
        $data['reports'] = CloudMigrationReport::where('user_id',$id)->where('is_prior_to_pay_as_you_go', 2)->whereNotNull('timestamp')->whereBetween('timestamp', [$from, $to])->orderBy('timestamp','DESC')->get();

        if(count($data['reports']) < 1 )
        {
            Session::flash('flash_danger', 'No record found for this user.');
            return redirect()->back(); 
        }

        //MAKE DIRECTORY
        $upload_path = 'public/temp/user-cloud-migration-reports';
        if (!File::exists(public_path() . '/storage/temp/user-cloud-migration-reports')) 
        {
            Storage::makeDirectory($upload_path);
        }

        # loop through each file
        foreach ($data['reports'] as $key=> $data['report']) {
            try {
                $data['files'] = $data['report']->files;
                $pdf = LaravelPDF::loadView('admin.lawful-interception.cloud_migration_report', $data);    
                $reportFilename = $user->email.'-cloud-migration-report-'.($key+1).'.pdf';

                // # download file
                Storage::put('public/temp/user-cloud-migration-reports/'.$reportFilename, $pdf->output());

            } catch (\Exception $e) {

            }
        }

        # create new zip object
        $zip = new ZipArchive();

        # create a temp file & open it
        $tmp_file = tempnam(public_path().'/storage/temp/', '');
        $zip->open($tmp_file, ZipArchive::CREATE);

        # get file from storage
        $files = File::files(public_path() . '/storage/temp/user-cloud-migration-reports');

        # loop through each file
        foreach ($files as $key => $value) {
            try {
                $relativeNameInZipFile = basename($value);

                #add it to the zip
                $zip->addFile($value, $relativeNameInZipFile);
            } catch (\Exception $e) {

            }
        }

        # close zip
        $zip->close();

        $_SESSION['LawfulUserDownloadFiles'] = 1;

        # delete directory
        if (File::exists(public_path() . '/storage/temp/user-cloud-migration-reports'))
        {
            File::deleteDirectory(public_path() . '/storage/temp/user-cloud-migration-reports');
        }

        $filename = $user->email.'-cloud-migration-reports-'.date('Y-m-d').'.zip';

        # send the file to the browser as a download
        header('Content-disposition: attachment; filename="'.$filename.'"');
        header('Content-type: application/zip');
        readfile($tmp_file);
        unlink($tmp_file);
    }

    public function mailboxMigrationReports($id, Request $request)
    {
        if(!have_right(92))
            access_denied();

            $lang = !empty(session('lang')) && session('lang') != 'en' ? session('lang') : 'en';
            $lang_file = public_path('i18n/translations/'.$lang.'.json');
            $lang_arr = json_decode(file_get_contents($lang_file),true);
    
            $data['lang_arr'] = $lang_arr;  
            $data['global_font_family'] = in_array($lang, ['ru','ja','zh']) ? 'chinesefont' : 'arial';

            $from = strtotime($request->from.' 00:00:00');
            $to = strtotime($request->to.' 23:59:59');
    
            $id = Hashids::decode($id)[0];
            $user = User::find($id);

            $data['details'] = MailboxMigrationReport::where('user_id',$id)->where('is_prior_to_pay_as_you_go', 2)->whereNotNull('timestamp')->whereBetween('timestamp', [$from, $to])->orderBy('timestamp','DESC')->get();
    
            if(count($data['details']) < 1 )
            {
                Session::flash('flash_danger', 'No record found for this user.');
                return redirect()->back(); 
            }
    
            //MAKE DIRECTORY
            $upload_path = 'public/temp/user-mailbox-migration-reports';
            if (!File::exists(public_path() . '/storage/temp/user-mailbox-migration-reports')) 
            {
                Storage::makeDirectory($upload_path);
            }
    
            $mailboxMigrationController = new MailboxMigrationController();
            
            # loop through each file
            foreach ($data['details'] as $key=> $data['detail']) {
                try {
                    if($data['detail']->source_server_type == 'imap' && $data['detail']->output <> '')
                    {
                        $data['report'] = $mailboxMigrationController->parse_report_imap($data['detail']->output);
                    }
                    else if($data['detail']->source_server_type == 'pop' && $data['detail']->output <> '')
                    {
                        $data['report'] = $mailboxMigrationController->parse_report_pop($data['detail']->output);
                    }
                    else
                    {
                        continue;
                    }

                    $pdf = LaravelPDF::loadView('admin.lawful-interception.mailbox_migration_report', $data);    
                    $reportFilename = $user->email.'-mailbox-migration-report-'.($key+1).'.pdf';
    
                    // # download file
                    Storage::put('public/temp/user-mailbox-migration-reports/'.$reportFilename, $pdf->output());
    
                } catch (\Exception $e) {
    
                }
            }
    
            # create new zip object
            $zip = new ZipArchive();
    
            # create a temp file & open it
            $tmp_file = tempnam(public_path().'/storage/temp/', '');
            $zip->open($tmp_file, ZipArchive::CREATE);
    
            # get file from storage
            $files = File::files(public_path() . '/storage/temp/user-mailbox-migration-reports');
    
            # loop through each file
            foreach ($files as $key => $value) {
                try {
                    $relativeNameInZipFile = basename($value);
    
                    #add it to the zip
                    $zip->addFile($value, $relativeNameInZipFile);
                } catch (\Exception $e) {
    
                }
            }
    
            # close zip
            $zip->close();

            $_SESSION['LawfulUserDownloadFiles'] = 1;
    
            # delete directory
            if (File::exists(public_path() . '/storage/temp/user-mailbox-migration-reports'))
            {
                File::deleteDirectory(public_path() . '/storage/temp/user-mailbox-migration-reports');
            }
    
            $filename = $user->email.'-mailbox-migration-reports-'.date('Y-m-d').'.zip';
    
            # send the file to the browser as a download
            header('Content-disposition: attachment; filename="'.$filename.'"');
            header('Content-type: application/zip');
            readfile($tmp_file);
            unlink($tmp_file);
    }

    public function userPaymentsPdf($id, Request $request)
    {
        if(!have_right(93))
            access_denied();

        $from = strtotime($request->from.' 00:00:00');
        $to = strtotime($request->to.' 23:59:59');

        $id = Hashids::decode($id)[0];
        $user = User::find($id);
        $data['payments'] = Payment::where('user_id',$id)->where('is_prior_to_pay_as_you_go', 2)->whereNotNull('timestamp')->whereBetween('timestamp', [$from, $to])->orderBy('timestamp','DESC')->get();

        if(count($data['payments']) > 0 )
        {
            $pdf = LaravelPDF::loadView('admin.lawful-interception.user_payments_pdf', $data);
            return $pdf->stream($user->email.'-payments.pdf');
        }
        else{
            Session::flash('flash_danger', 'No record found for this user.');
            return redirect()->back(); 
        } 
    }

    public function userSubscriptionsPdf($id, Request $request)
    {
        if(!have_right(94))
            access_denied();

        $from = $request->from.' 00:00:00';
        $to = $request->to.' 23:59:59';

        $id = Hashids::decode($id)[0];
        $user = User::find($id);
        $data['subscriptions'] = PackageSubscription::where('user_id',$id)->where('is_prior_to_pay_as_you_go', 2)->whereBetween('created_at', [$from, $to])->orderBy('created_at','DESC')->get();

        if(count($data['subscriptions']) > 0 )
        {
            $pdf = LaravelPDF::loadView('admin.lawful-interception.user_subscriptions_pdf', $data);
            return $pdf->stream($user->email.'-subscriptions.pdf');
        }
        else{
            Session::flash('flash_danger', 'No record found for this user.');
            return redirect()->back(); 
        } 
    }

    public function checkUserTempFile($id)
    {
        $id = Hashids::decode($id)[0];
        $user = User::find($id);
        $userfilename = $user->temp_zip_file;
        if(isset($userfilename))
        {
            return response()->json([
                'status' => 1,
                'message' => 'File name is saved.'
            ], 200, ['Content-Type' => 'application/json']);
        }
        else
        {
            return response()->json([
                'status' => 2,
                'message' => 'File name not saved yet.'
            ], 200, ['Content-Type' => 'application/json']);
        }
    }

    public function archiveUserData($id, Request $request)
    {
        if(!have_right(95))
            access_denied();

        $id = Hashids::decode($id)[0];
        $user = User::find($id);
        
        $userfilename = $user->temp_zip_file;
        if(!empty($userfilename))
        {
            $tmp_file = public_path() . '/storage/temp/' . $userfilename;
            if(file_exists($tmp_file)) 
            {
                unlink($tmp_file);
            }

            $user->update([
                'temp_zip_file' => null
            ]);
        }

        # delete directory
        if (File::exists(public_path() . '/storage/temp/user-data-'.$user->email))
        {
            File::deleteDirectory(public_path() . '/storage/temp/user-data-'.$user->email);
        }
        
        ArchiveUsersDataJob::dispatch($id, $request->all());

        return response()->json([
            'status' => 1,
            'action' => 'user-all-data',
            'message' => 'Job processing.'
        ], 200, ['Content-Type' => 'application/json']);
    }

    public function downloadAllData($id)
    {
        if(!have_right(95))
            access_denied();

        // session_start();
        $id = Hashids::decode($id)[0];
        $user = User::find($id);
        $userfilename = $user->temp_zip_file;

        if(isset($userfilename))
        {
            $tmp_file = public_path() . '/storage/temp/' . $userfilename;

            # delete directory
            if (File::exists(public_path() . '/storage/temp/user-data-'.$user->email))
            {
                File::deleteDirectory(public_path() . '/storage/temp/user-data-'.$user->email);
            }

            $filename = $user->email.'-data-'.date('Y-m-d').'.zip';

            if(file_exists($tmp_file)) 
            {
                # send the file to the browser as a download
                header('Content-disposition: attachment; filename="'.$filename.'"');
                header('Content-type: application/zip');
                readfile($tmp_file);
                unlink($tmp_file);
                $user->update([
                    'temp_zip_file' => null
                ]);
            }
            else
            {
                Session::flash('flash_danger', 'No record found for this user.');
                return redirect()->back(); 
            }
        }
        else
        {
            Session::flash('flash_danger', 'No record found for this user.');
            return redirect()->back(); 
        }
    }

    public function getLawfulUserDownloadFilesSession(Request $request) {
        if(isset($_SESSION['LawfulUserDownloadFiles']) && $_SESSION['LawfulUserDownloadFiles'] == 1)
        {
            unset($_SESSION['LawfulUserDownloadFiles']);
            return response()->json(['status' => 2]);
        }
        else {
            return response()->json(['status' => 1]);
        }
    }

}
