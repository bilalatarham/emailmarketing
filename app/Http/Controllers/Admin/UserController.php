<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\User;
use App\Models\Language;
use App\Models\Timezone;
use App\Models\Country;
use App\Models\Package;
use App\Models\PackageSubscription;
use App\Models\Payment;
use App\Models\EmailTemplate;
use App\Models\CloudMigrationReport;
use App\Models\MailboxMigrationReport;
use App\Classes\PaymentHandler;
use Carbon\Carbon;
use Auth;
use Hashids;
use File;
use Storage;
use Session;
use Hash;
use DB;
use DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if(!have_right(11))
            access_denied();

        if($request->ajax())
        {
            $db_record = new User();

            if($request->has('package_id') && !empty($request->package_id))
            {
                $db_record = $db_record->where('package_id',$request->package_id);
            }

            if($request->has('platform') && !empty($request->platform))
            {
                $db_record = $db_record->where('platform', $request->platform);
            }

            if($request->has('status') && $request->status != "")
            {
                $db_record = $db_record->where('status', $request->status);
            }

            $db_record =  $db_record->orderBy('created_at','DESC');
            $datatable = Datatables::of($db_record);
            $datatable = $datatable->addIndexColumn();
            $datatable = $datatable->addColumn('package_title', function($row)
            {
                $package_title = 'N/A';
                if(isset($row->package->title))
                {
                    $package_title = $row->package->title;
                }
                return $package_title;
            });

            $datatable = $datatable->editColumn('platform', function($row)
            {
                $platform = 'Web';
                
                switch ($row->platform) {
                    case 2:
                        $platform = 'Mobile';
                        break;
                    case 3:
                        $platform = 'Thunderbird';
                        break;
                    case 4:
                        $platform = 'Outlook';
                        break;
                    case 5:
                        $platform = 'Transfer Immunity';
                        break;
                    case 6:
                        $platform = 'Ned Link';
                        break;
                    case 7:
                        $platform = 'aikQ';
                        break;
                    case 8:
                        $platform = 'Inbox';
                        break;
                    case 9:
                        $platform = 'Overmail';
                        break;
                    case 10:
                        $platform = 'Maili';
                        break;
                    case 11:
                        $platform = 'Product Immunity';
                        break;
                    case 12:
                        $platform = 'QR Code';
                        break;
                }

                return '<span class="label label-default">'.$platform.'</span>';
            });
            $datatable = $datatable->editColumn('status', function($row)
            {
                $status = '<span class="label label-danger">Disable</span>';
                if ($row->status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                }
                else if ($row->status == 2)
                {
                    $status = '<span class="label label-warning">Unverified</span>';
                }
                else if ($row->status == 3)
                {
                    $status = '<span class="label label-danger">Deleted</span>';
                }
                return $status;
            });
            $datatable = $datatable->addColumn('action', function($row)
            {
                $actions = '<span class="actions">';

                if(have_right(15))
                {
                    $actions .= '&nbsp;<a title="Cloud Accounts" class="btn btn-primary" href="'.url("admin/users/cloud-accounts/" . Hashids::encode($row->id)).'"><i class="fa fa-cloud"></i></a>';
                }

                if(have_right(16))
                {
                    $actions .= '&nbsp;<a title="Cloud Migrations Report" class="btn btn-primary" href="'.url("admin/users/cloud-migrations-report/" . Hashids::encode($row->id)).'"><i class="fa fa-file"></i></a>';
                }

                if(have_right(17))
                {
                    $actions .= '&nbsp;<a title="Mailbox Migrations Report" class="btn btn-primary" href="'.url("admin/users/mailbox-migrations-report/" . Hashids::encode($row->id)).'"><i class="fa fa-file"></i></a>';
                }

                if(have_right(18))
                {
                    $actions .= '&nbsp;<a title="Payments" class="btn btn-primary" href="'.url("admin/users/payments/" . Hashids::encode($row->id)).'"><i class="fa fa-credit-card-alt"></i></a>';
                }

                if(have_right(19))
                {
                    $actions .= '&nbsp;<a title="Subscriptions" class="btn btn-primary" href="'.url("admin/users/subscriptions/" . Hashids::encode($row->id)).'"><i class="fa fa-tasks"></i></a>';
                }

                if(have_right(13))
                {
                    $actions .= '&nbsp;<a class="btn btn-primary" href="'.url("admin/users/" . Hashids::encode($row->id).'/edit').'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>';
                }

                if(have_right(85))
                {
                    $actions .= '&nbsp;<a class="btn btn-primary" href="'.url("admin/users/packages/" . Hashids::encode($row->id)).'" title="Update Package"><i class="fa fa-sliders"></i></a>';
                }
                
                if(have_right(14))
                {
                    $actions .= '&nbsp;<form method="POST" action="'.url("admin/users/" . Hashids::encode($row->id)).'" accept-charset="UTF-8" style="display:inline">';
                    $actions .= '<input type="hidden" name="_method" value="DELETE">';
                    $actions .= '<input name="_token" type="hidden" value="'.csrf_token().'">';
                    $actions .= '<button class="btn btn-danger" onclick="return confirm(\'Are you sure you want to delete this record?\');" title="Delete">';
                    $actions .= '<i class="fa fa-trash"></i>';
                    $actions .= '</button>';
                    $actions .= '</form>';
                }

                $actions .= '</span>';
                return $actions;
            });

            $datatable = $datatable->rawColumns(['platform','status','action']);
            $datatable = $datatable->make(true);
            return $datatable;
        }
        
        $data = [];
        $package_id = '';

        if($request->has('package_id') && !empty($request->package_id))
        {
            $package_id = Hashids::decode($request->package_id)[0];
        }

        $data['package_id'] = $package_id;
        $data['packages'] = Package::all();

        return view('admin.users.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!have_right(12))
            access_denied();

        $data['user'] = new User();
        $data['languages'] = Language::where('status',1)->whereNull('deleted_at')->get();
        $data['timezones'] = Timezone::all();
        $data['countries'] = Country::where('status',1)->get();
        $data['action'] = "Add";
        return view('admin.users.form')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!have_right(13))
            access_denied();

        $id = Hashids::decode($id)[0];
        $data['action'] = "Edit";
        $data['user'] = User::findOrFail($id);
        $data['languages'] = Language::where('status',1)->whereNull('deleted_at')->get();
        $data['timezones'] = Timezone::all();
        $data['countries'] = Country::where('status',1)->get();
        return view('admin.users.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if($input['action'] == 'Add')
        {
            $validator = Validator::make($request->all(), [
                'email' => ['required','string','max:100',Rule::unique('users')],
                'username' => ['required','string','max:100',Rule::unique('users')],
                'name' => ['required','string','max:100'],
                'password' => 'required|string|min:8|max:30',
                'country' => 'required'
            ]);

            if ($validator->fails())
            {
                Session::flash('flash_danger', $validator->messages());
                return redirect()->back()->withInput();
            }

            $input['original_password'] = $input['password'];
            $input['password'] = Hash::make($input['password']);
            $input['language'] = "en";
            $input['country_id'] = $input['country'];

            $model = new User();
            $flash_message = 'User has been created successfully.';
        }
        else
        {
            $validator = Validator::make($request->all(), [
                'email' => ['required','string',Rule::unique('users')->ignore($input['id'])],
                'username' => ['required','string',Rule::unique('users')->ignore($input['id'])],
                'password' => 'required|string|min:8|max:30',
                'country' => 'required'
            ]);

            if ($validator->fails())
            {
                Session::flash('flash_danger', $validator->messages());
                return redirect()->back()->withInput();
            }

            if(!empty($input['password']))
            {
                $input['original_password'] = $input['password'];
                $input['password'] = Hash::make($input['password']);
            }
            else
            {
                unset($input['password']);
            }

            $input['country_id'] = $input['country'];

            $model = User::findOrFail($input['id']);
            $flash_message = 'User has been updated successfully.';
        }

        $model->fill($input);
        $model->disabled_at = ($input['status'] == "0") ? date("Y-m-d H:i:s") : Null;
        $model->deleted_at = ($input['status'] == "3") ? date("Y-m-d H:i:s") : Null;
        $model->save();

        if($input['action'] == 'Add')
        {
            $package = Package::where(['id' => 1, 'status' => 1])->first(); // Trial Package
            $end_date = Carbon::now('UTC')->addDays(settingValue('number_of_days'))->timestamp;
            $on_trial = 1;

            if(empty($package) || (!empty($package) && settingValue('number_of_days') == 0)) // Trial is not active
            {
                $package = Package::find(2); // Free Package
                $end_date = Null;
                $on_trial = 0;
            }

            $packageLinkedFeatures = $package->linkedFeatures->pluck('count','feature_id')->toArray();

            if(array_key_exists(1, $packageLinkedFeatures))
            {
                $model->total_migrations = $model->remaining_migrations = $packageLinkedFeatures[1];
            }

            $packageSubscription = PackageSubscription::create([
                'user_id'               =>  $model->id,
                'package_id'            =>  $package->id,
                'per_migration_price'   =>  0,
                'price'                 =>  0,
                'features'              =>  empty($package->linkedFeatures) ? '' : json_encode($packageLinkedFeatures),
                'start_date'            =>  Carbon::now('UTC')->timestamp,
                'end_date'              =>  $end_date,
                'is_active'             =>  1
            ]);

            $model->package_id = $package->id;
            $model->package_subscription_id = $packageSubscription->id;
            $model->on_trial = $on_trial;
            $model->save();
        }

        $request->session()->flash('flash_success', $flash_message);
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        if(!have_right(14))
            access_denied();

        $id = Hashids::decode($id)[0];
        User::destroy($id);
        Session::flash('flash_success', 'User has been deleted successfully.');
        
        if($request->has('page') && $request->page == 'dashboard' )
        {
            return redirect('admin/dashboard');
        }
        else
        {
        return redirect('admin/users');
        }
    }
    
     /**
     * Show the user subscription.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function subscriptions(Request $request,$id)
    {
        if(!have_right(19))
            access_denied();

        $data = [];
        $data['id'] = $id;
        $id = Hashids::decode($id)[0];
        $data['user'] = User::find($id);
        
        if ($request->ajax())
        {
            $db_record = PackageSubscription::where('is_prior_to_pay_as_you_go', 2)->with('package');

            if($request->has('search') && !empty($request->search))
            {
                $db_record = $db_record->whereHas('package', function($q) use ($request) {
                    $q->where('title', 'LIKE', '%' . $request->search . '%');
                })->where('user_id',$id);
            }
            else
            {
                
                $db_record =  $db_record->whereNotNull('id');
            }

            $db_record = $db_record->where('user_id', $id);
            $db_record = $db_record->orderBy('created_at','DESC');
            
            $datatable = Datatables::of($db_record);

            $datatable = $datatable->addIndexColumn();
            $datatable = $datatable->addColumn('package_title', function($row)
            {
                return $row->package_title;
            });
            $datatable = $datatable->editColumn('price', function($row)
            {
                return '<sup>'.config('constants.currency')['symbol'].'</sup>'.$row->price;
            });
            $datatable = $datatable->addColumn('type', function($row)
            {
                $type = '';

                if($row->package_id == 1)
                    $type = '<span class="label label-primary">Trial</span>';
                else if($row->package_id == 2)
                    $type = '<span class="label label-primary">Free</span>';
                else
                {
                    $type = '<span class="label label-success">Paid</span>';
                } 

                return $type;
            });
            $datatable = $datatable->editColumn('start_date', function($row)
            {
                return Carbon::createFromTimeStamp($row->start_date, "UTC")->tz(session('timezone'))->format('d M, Y') ;
            });
            $datatable = $datatable->editColumn('end_date', function($row)
            {
                return (!empty($row->end_date)) ? Carbon::createFromTimeStamp($row->end_date, "UTC")->tz(session('timezone'))->format('d M, Y') : 'Lifetime' ;
            });
            $datatable = $datatable->addColumn('status', function($row)
            {
                $currentTimestamp = Carbon::now('UTC')->timestamp;
                $status = '';
                
                if($row->id == $row->user->package_subscription_id)
                {
                    if(empty($row->end_date) || $row->end_date > $currentTimestamp)
                        $status = '<span class="label label-success">Active</span>';
                    else
                        $status = '<span class="label label-warning">Expired</span>';
                }   
                else
                    $status = '<span class="label label-danger">In-Active</span>';
                
                return $status;
            });
            
            $datatable = $datatable->rawColumns(['type','price','status']);
            $datatable = $datatable->make(true);
            return $datatable;
        }

        return view('admin.users.subscriptions',$data);
    }

    public function payments(Request $request,$id)
    {
        if(!have_right(18))
            access_denied();

        $data = [];
        $data['id'] = $id;
        $id = Hashids::decode($id)[0];
        $data['user'] = User::find($id);
        
        if ($request->ajax())
        {
            $db_record = Payment::where('user_id',$id)->where('is_prior_to_pay_as_you_go', 2)->whereNotNull('timestamp')->orderBy('timestamp','DESC');//->get();

            if($request->has('search') && !empty($request->search))
            {
                $db_record = $db_record->where('item', 'LIKE', '%' . $request->search . '%');
            }
            else
            {
                
                $db_record =  $db_record->whereNotNull('id');
            }

            $datatable = Datatables::of($db_record);
            $datatable = $datatable->addIndexColumn();

            // $datatable = $datatable->editColumn('item', function($row)
            // {
            //     $payload = json_decode($row->payload,true);
            //     // return $payload["subscription_desc"];
            //     return $row->item;
            // });

            $datatable = $datatable->editColumn('amount', function($row)
            {
                return '<sup>'.config('constants.currency')['symbol'].'</sup>'.$row->amount;
            });

            $datatable = $datatable->editColumn('vat_amount', function($row)
            {
                return '<sup>'.config('constants.currency')['symbol'].'</sup>'.$row->vat_amount;
            });

            $datatable = $datatable->editColumn('discount_amount', function($row)
            {
                return '<sup>'.config('constants.currency')['symbol'].'</sup>'.$row->discount_amount;
            });

            $datatable = $datatable->editColumn('total_amount', function($row)
            {
                return '<sup>'.config('constants.currency')['symbol'].'</sup>'.$row->total_amount;
            });

            $datatable = $datatable->editColumn('payment_method', function($row)
            {
                $payment_method = '';

                switch ($row->payment_method) {
                    case config('constants.payment_methods')['PAYPAL']:
                        $payment_method = '<span class="label label-primary">Paypal</span>';
                        break;
                    case config('constants.payment_methods')['WIRECARD']:
                        $payment_method = '<span class="label label-primary">Wirecard</span>';
                        break;
                    case config('constants.payment_methods')['KLARNA']:
                        $payment_method = '<span class="label label-primary">Klarna</span>';
                        break;
                    case config('constants.payment_methods')['ALIPAY']:
                        $payment_method = '<span class="label label-primary">Alipay</span>';
                        break;
                    case config('constants.payment_methods')['AMAZONPAY']:
                        $payment_method = '<span class="label label-primary">Amazon Pay</span>';
                        break;
                    case config('constants.payment_methods')['MOLLIE']:
                        $payment_method = '<span class="label label-primary">Mollie</span>';
                        break;
                    case config('constants.payment_methods')['ADMIN']:
                        $payment_method = '<span class="label label-primary">Admin</span>';
                        break;
                    case config('constants.payment_methods')['VOUCHER_PROMOTION']:
                        $payment_method = '<span class="label label-primary">Voucher Promotion</span>';
                        break;
                }

                return $payment_method;
            });

            $datatable = $datatable->addColumn('payment_date', function($row)
            {
                return Carbon::createFromTimeStamp($row->timestamp, "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') ;
            });

            $datatable = $datatable->addColumn('action', function($row)
            {
                $actions = '<span class="actions">';

                if(have_right(104))
                {
                    $actions .= '&nbsp;<a title="Download Invoice" class="btn btn-primary" href="'.url("/api/subscription/download-payment-invoice/" . Hashids::encode($row->id)).'"><i class="fa fa-download"></i></a>';
                }

                $actions .= '</span>';
                return $actions;
            });
            
            $datatable = $datatable->rawColumns(['amount','vat_amount','discount_amount','total_amount','payment_method','action']);
            $datatable = $datatable->make(true);
            return $datatable;
        }

        return view('admin.users.payments',$data);
    }

    public function cloudAccounts(Request $request,$id)
    {
        if(!have_right(15))
            access_denied();

        $data = [];
        $data['id'] = $id;
        $data['user'] = $user = User::find(Hashids::decode($id)[0]);
        
        if ($request->ajax())
        {
            $db_record = $user->cloudAccounts;

            $datatable = Datatables::of($db_record);
            $datatable = $datatable->addIndexColumn();

            $datatable = $datatable->addColumn('cloud_image', function($row)
            {
                return '<img src="'.asset(getCloudMetaData($row->cloud_id)['smallImage']).'" />';
            });

            $datatable = $datatable->addColumn('cloud_name', function($row)
            {
                return getCloudMetaData($row->cloud_id)['name'];
            });

            $datatable = $datatable->editColumn('total_space', function($row)
            {
                return convertBytesToGigaBytes($row->total_space).' GB';
            });

            $datatable = $datatable->editColumn('used_space', function($row)
            {
                return convertBytesToGigaBytes($row->used_space).' GB';
            });

            $datatable = $datatable->editColumn('available_space', function($row)
            {
                return convertBytesToGigaBytes($row->available_space).' GB';
            });

            $datatable = $datatable->editColumn('created_at', function($row)
            {
                return Carbon::createFromTimeStamp(strtotime($row->created_at), "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') ;
            });
            
            $datatable = $datatable->rawColumns(['cloud_image','cloud_name','total_space','used_space','available_space','created_at']);
            $datatable = $datatable->make(true);
            return $datatable;
        }

        return view('admin.users.cloud_accounts',$data);
    }

    public function cloudMigrationsReport(Request $request,$id)
    {
        if(!have_right(16))
            access_denied();

        $data = [];
        $data['id'] = $id;
        $data['user'] = $user = User::find(Hashids::decode($id)[0]);
        
        if ($request->ajax())
        {
            $db_record = $user->cloudMigrationsReport->sortByDesc('created_at');

            $datatable = Datatables::of($db_record);
            $datatable = $datatable->addIndexColumn();

            $datatable = $datatable->addColumn('source_cloud', function($row)
            {
                $cloudMetaData = getCloudMetaData($row->sourceCloud->cloud_id);
                return '<img src="'.asset($cloudMetaData['smallImage']).'" /> '.$row->sourceCloud->email;
            });

            $datatable = $datatable->addColumn('destination_cloud', function($row)
            {
                $cloudMetaData = getCloudMetaData($row->destinationCloud->cloud_id);
                return '<img src="'.asset($cloudMetaData['smallImage']).'" /> '.$row->destinationCloud->email;
            });

            $datatable = $datatable->editColumn('status', function($row)
            {
                $status = '';
                if ($row->status == 1)
                {
                    $status = '<span class="label label-warning">In Queue</span>';
                }
                else if ($row->status == 2)
                {
                    $status = '<span class="label label-success">Processed</span>';
                }
                return $status;
            });

            $datatable = $datatable->editColumn('processed_on', function($row)
            {
                return Carbon::createFromTimeStamp($row->processed_on, "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') ;
            });

            $datatable = $datatable->editColumn('created_at', function($row)
            {
                return Carbon::createFromTimeStamp($row->timestamp, "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') ;
            });

            $datatable = $datatable->addColumn('action', function($row)
            {
                $actions = '<span class="actions">';

                if(have_right(82))
                {
                    $actions .= '&nbsp;<a title="View Detail" class="btn btn-primary" href="'.url("admin/users/cloud-migration-report-detail/" . Hashids::encode($row->id)).'"><i class="fa fa-eye"></i></a>';
                }

                if(have_right(83))
                {
                    $actions .= '&nbsp;<a title="Download Report" class="btn btn-primary" href="'.url("api/clouds/download-migration-report/" . Hashids::encode($row->id)).'"><i class="fa fa-download"></i></a>';
                }

                $actions .= '</span>';
                return $actions;
            });
            
            $datatable = $datatable->rawColumns(['source_cloud','destination_cloud','status','processed_on','created_at','action']);
            $datatable = $datatable->make(true);
            return $datatable;
        }

        return view('admin.users.cloud_migrations_report',$data);
    }

    public function cloudMigrationReportDetail(Request $request,$id)
    {
        if(!have_right(82))
            access_denied();

        $data = [];
        $data['id'] = $id;
        $data['report'] = $report = CloudMigrationReport::find(Hashids::decode($id)[0]);
        
        if ($request->ajax())
        {
            $db_record = $report->files;

            $datatable = Datatables::of($db_record);
            $datatable = $datatable->addIndexColumn();

            $datatable = $datatable->editColumn('size', function($row)
            {
                return empty($row->size) ? '-' : formatBytes($row->size);
            });

            $datatable = $datatable->editColumn('status', function($row)
            {
                $status = '';
                if ($row->status == 1)
                {
                    $status = '<span class="label label-warning">In Queue</span>';
                }
                else if ($row->status == 2)
                {
                    $status = '<span class="label label-success">Processed</span>';
                }
                return $status;
            });

            $datatable = $datatable->editColumn('processed_on', function($row)
            {
                return Carbon::createFromTimeStamp($row->processed_on, "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') ;
            });

            $datatable = $datatable->editColumn('created_at', function($row)
            {
                return Carbon::createFromTimeStamp($row->timestamp, "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') ;
            });
            
            $datatable = $datatable->rawColumns(['size','status','processed_on','created_at']);
            $datatable = $datatable->make(true);
            return $datatable;
        }

        return view('admin.users.cloud_migration_report_detail',$data);
    }

    public function mailboxMigrationsReport(Request $request,$id)
    {
        if(!have_right(17))
            access_denied();

        $data = [];
        $data['id'] = $id;
        $data['user'] = $user = User::find(Hashids::decode($id)[0]);
        
        if ($request->ajax())
        {
            $db_record = $user->mailboxMigrationsReport->sortByDesc('created_at');

            $datatable = Datatables::of($db_record);
            $datatable = $datatable->addIndexColumn();

            $datatable = $datatable->editColumn('status', function($row)
            {
                $status = '';
                if ($row->status == 1)
                {
                    $status = '<span class="label label-warning">In Queue</span>';
                }
                else if ($row->status == 2)
                {
                    $status = '<span class="label label-success">Processed</span>';
                }
                return $status;
            });

            $datatable = $datatable->editColumn('processed_on', function($row)
            {
                return Carbon::createFromTimeStamp($row->processed_on, "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') ;
            });

            $datatable = $datatable->editColumn('created_at', function($row)
            {
                return Carbon::createFromTimeStamp($row->timestamp, "UTC")->tz(session('timezone'))->format('d M, Y - h:i A') ;
            });

            $datatable = $datatable->addColumn('action', function($row)
            {
                $actions = '<span class="actions">';

                if(have_right(84))
                {
                    $actions .= '&nbsp;<a title="Download Report" class="btn btn-primary" href="'.url("api/mailbox/download-migration-report/" . Hashids::encode($row->id)).'"><i class="fa fa-download"></i></a>';
                }

                $actions .= '</span>';
                return $actions;
            });
            
            $datatable = $datatable->rawColumns(['status','action']);
            $datatable = $datatable->make(true);
            return $datatable;
        }

        return view('admin.users.mailbox_migrations_report',$data);
    }

    public function packages(Request $request,$id)
    {
        if(!have_right(85))
            access_denied();

        $user = User::find(Hashids::decode($id)[0]);
        $data['user'] = $user;
        $data['packages'] = Package::whereNotIn('id',[1])->where('status',1)->orderBy('total_price')->get();

        $subscription = $user->subscription;
        $currentTimestamp = Carbon::now('UTC')->timestamp;

        if(!empty($subscription->end_date) && $subscription->end_date < $currentTimestamp)
        {
            $user->update([
                'on_hold_package_id' =>  $subscription->package_id,
                'prev_package_subscription_id' => $subscription->id
            ]);
        }

        return view('admin.users.packages',$data);
    }

    public function updatePackage(Request $request)
    {
        $user = User::find($request->user_id);
        $name = $user->name;
        $email = $user->email;

        if($request->payment_option == 1)
        {
            PaymentHandler::checkout($request,$request->package_id,$request->user_id,7,'','','','Admin');
            Session::flash('flash_success', 'Package has been updated successfully.');
        }
        else
        {   
            $payment_link = url('/payment-checkout?package_id='.$request->package_id);
            $email_template = EmailTemplate::where('type','unpaid_package_upgrade_downgrade_by_admin')->first();
            $subject = $email_template->subject;
            $content = $email_template->content;
    
            $search = array("{{name}}","{{link}}","{{app_name}}");
            $replace = array($name,$payment_link,env('APP_NAME'));
            $content  = str_replace($search,$replace,$content);
    
            sendEmail($email, $subject, $content);
            Session::flash('flash_success', 'Package change request has been initiated successfully.');
            $user->update([
                'package_updated_by_admin'  => 0,
                'unpaid_package_email_by_admin' => 1
            ]);
        }
        
        return redirect()->back();
    }

    public function sendPassword($id)
    {
        $user = User::find(Hashids::decode($id)[0]);
        $name = $user->name;
        $email = $user->email;

        $email_template = EmailTemplate::where('type','send_password')->first();
        $subject = $email_template->subject;
        $content = $email_template->content;

        $search = array("{{name}}","{{password}}","{{app_name}}");
        $replace = array($name,$user->original_password,env('APP_NAME'));
        $content  = str_replace($search,$replace,$content);

        sendEmail($email, $subject, $content);

        Session::flash('flash_success', 'Password has been sent successfully.');
        return redirect('admin/users/'.$id.'/edit');
    }
}