<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use DB;
use App\Models\Notification;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
    	$data['roles'] = '';
    	$data['admins'] = 0;
    	$data['users'] = DB::table('users')->count();
    	$data['packages'] = 0;
    	$data['faqs'] = DB::table('faqs')->count();
    	$data['languages'] = DB::table('languages')->count();
        $data['email_templates'] = DB::table('email_templates')->count();
        $data['cms_pages'] = DB::table('cms_pages')->count();
        $data['cloud_accounts'] = 0;
        $data['cloud_migration_reports'] = 0;
        $data['mailbox_migration_reports'] = 0;
        $data['deleted_users'] = [];//]User::where('status',3)->orderBy('name','DESC')->get();
        $data['received_payment'] =0;

        return view('admin.dashboard')->with($data);
    }

    public function ajaxReceivedNotification(Request $request)
    {
        $notification = Notification::find($request->id);
        if($notification && $notification->user)
        {
            $message = str_replace("[name]" , $notification->user->name , $notification->message );

            $html = '<li><a href="'.url($notification->link.'?notification_id='.$notification->id).'" class="notification-item" style="background:#e4edfc"><i class="fa fa-tags custom-bg-green2"></i><p><span class="text">'.$message.'</span><span class="timestamp">'.Carbon::createFromTimeStamp(strtotime($notification->created_at), "UTC")->diffForHumans().'</span></p></a></li>';
            
            return response()->json(['success'=>true, 'html' => $html]);
        } 
    }
}
