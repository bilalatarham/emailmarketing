<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\User;
use App\Models\Package;
use App\Models\PackageSubscription;
use App\Models\Payment;
use Carbon\Carbon;

class PaymentController extends Controller
{
    public function createDataPayload($package_id,$type)
    {
        $package = Package::find($package_id);
        $subscription_type = ($type == 1) ? 'Monthly' : 'Yearly';
        $subscription_description = $subscription_type.' Subscription ('.$package->sub_title.')';

        $data = [];

        $data['items'] = [
            [
                'name'  => $package->title,
                'desc'  => $subscription_description,
                'price' => ($type == 1) ? $package->monthly_price : $package->yearly_price * 12,
                'qty'   => 1,
            ],
        ];

        $data['subscription_desc'] = $subscription_description;
        $data['invoice_id'] = uniqid();
        $data['invoice_description'] = $subscription_description;

        $total = 0;
        foreach ($data['items'] as $item) {
            $total += $item['price'] * $item['qty'];
        }

        $data['total'] = $total;

        return $data;
    }

    public function paypalExpressCheckout()
    {
        $package_id = 2;
        $type = 1;
        $user_id = 9;

        // ************************** //
        //    Get Package Details     //
        // ************************** //

        $package = Package::find($package_id);

        // ************************** //
        //    Add New Subscription    //
        // ************************** //

        if($type == 1)
        {
            $end_date = Carbon::now('UTC')->addMonth()->timestamp;
            $price = $package->monthly_price;
        }
        else
        {
            $end_date = Carbon::now('UTC')->addYear()->timestamp;
            $price = $package->yearly_price * 12;
        }

        $packageSubscription = PackageSubscription::create([
            'user_id'               =>  $user_id,
            'package_id'            =>  $package_id,
            'per_migration_price'   =>  $package->per_unit_price,
            'type'                  =>  $type,
            'price'                 =>  $price,
            'features'              =>  empty($package->linkedFeatures) ? '' : implode(',', $package->linkedFeatures->pluck('feature_id')->toArray()),
            'start_date'            =>  Carbon::now('UTC')->timestamp,
            'end_date'              =>  ($price > 0) ? $end_date : NULL,
            'is_active'             =>  1
        ]);

        if($package->monthly_price == 0 && $package->yearly_price == 0)
        {
            // ************************** //
            //  Cancel Recurring Profile  //
            // ************************** //

            $user = User::find($user_id);

            $provider = new ExpressCheckout;
            $response = $provider->cancelRecurringPaymentsProfile($user->paypal_profile_id);

            $user->update([
                'package_subscription_id'  => $packageSubscription->id
            ]);
        }
        else
        {
            // ************************** //
            //        Add New Payment     //
            // ************************** //

            $payment = Payment::create([
                'user_id'           =>  $user_id,
                'subscription_id'   =>  $packageSubscription->id,
                'type'              =>  1, // Paypal
            ]);

            $data = $this->createDataPayload($package_id,$type);

            $data['return_url'] = url('/admin/payments/paypal-checkout-success?mode=recurring&package_id='.$package_id.'&type='.$type.'&payment_id='.$payment->id);
            $data['cancel_url'] = url('/admin/payments/paypal-checkout-cancel?subscription_id='.$packageSubscription->id);

            $provider = new ExpressCheckout; // To use express checkout.
            $response = $provider->setExpressCheckout($data, true);
            echo '<pre>';
            print_r($response);
            exit;

             // This will redirect user to PayPal
            return redirect($response['paypal_link']);
        }
    }

    public function paypalCheckoutSuccess()
    {
        $token = $_GET['token'];
        $package_id = $_GET['package_id'];
        $type = $_GET['type'];
        $payment_id = $_GET['payment_id'];

        $payment = Payment::find($payment_id);
        $user = User::find($payment->user_id);

        // ****** Get Express Checkout Detail ******* //

        $provider = new ExpressCheckout;
        $checkoutDetails = $provider->getExpressCheckoutDetails($token);
        // echo '<pre>';
        // print_r($checkoutDetails);
        // exit;

        if(isset($checkoutDetails['ACK']) && $checkoutDetails['ACK'] == 'Success')
        {
            // ****** Do Express Checkout Payment ******* //

            $data = $this->createDataPayload($package_id,$type);
            $paymentResponse = $provider->doExpressCheckoutPayment($data, $token, $checkoutDetails['PAYERID']);

            if(isset($paymentResponse['ACK']) && $paymentResponse['ACK'] == 'Success')
            {
                $payment->update([
                    'token'             =>  $token,
                    'payer_id'          =>  $checkoutDetails['PAYERID'],
                    'data'              =>  json_encode($paymentResponse),
                    'timestamp'         =>  Carbon::now('UTC')->timestamp
                ]);

                // ************************** //
                //  Cancel Recurring Profile  //
                // ************************** //

                if(!empty($user->paypal_profile_id))
                {
                    $response = $provider->cancelRecurringPaymentsProfile($user->paypal_profile_id);
                }

                // ************************** //
                //  Create Recurring Profile  //
                // ************************** //

                $amount = $data['total'];
                $description = $data['subscription_desc'];

                if($type == 1)
                {
                    $response = $provider->createMonthlySubscription($token, $amount, $description);
                }
                else
                {
                    $response = $provider->createYearlySubscription($token, $amount, $description);
                }

                $payment->update([
                    'profile_id'    => $response['PROFILEID'],
                    'profile_data'  => json_encode($response)
                ]);

                $user->update([
                    'paypal_profile_id'         => $response['PROFILEID'],
                    'package_subscription_id'   => $payment->subscription_id
                ]);

                echo '<pre>';
                print_r($response);
                exit;
            }
            else
            {
                PackageSubscription::find($payment->subscription_id)->delete();
            }
        }
        else
        {
            PackageSubscription::find($payment->subscription_id)->delete();
        }
    }

    public function paypalCheckoutCancel()
    {
        PackageSubscription::find($_GET['subscription_id'])->delete();
    }
}
