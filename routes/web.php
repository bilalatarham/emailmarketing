<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// ******************** //
//     Admin Routes
// ******************** //

Route::get('admin/login', [Auth\Admin\LoginController::class, 'login'])->name('admin.auth.login');
Route::post('admin/login', [Auth\Admin\LoginController::class, 'loginAdmin'])->name('admin.auth.loginAdmin');
Route::any('admin/logout', [Auth\Admin\LoginController::class, 'logout'])->name('admin.auth.logout');
Route::get('admin/forgot-password',  [Auth\Admin\ForgotPasswordController::class, 'forgotPasswordForm'])->name('admin.auth.forgot-password');
Route::post('admin/send-reset-link-email', [Auth\Admin\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('admin.auth.send-reset-link-email');
Route::get('admin/reset-password/{token}', [Auth\Admin\ForgotPasswordController::class, 'resetPasswordForm']);
Route::post('admin/reset-password', [Auth\Admin\ForgotPasswordController::class, 'resetPassword'])->name('admin.auth.reset-password');

Route::group(['namespace' => 'Admin', 'as' => 'admin.', 'prefix' => 'admin', 'middleware' => ['auth:admin','admin.check.status']], function ()
{
    Route::get('/', 'DashboardController@dashboard')->name('dashboard');
    Route::get('dashboard', 'DashboardController@dashboard')->name('dashboard');
    Route::post('ajax-received-notification', 'DashboardController@ajaxReceivedNotification');
    Route::get('settings', 'SettingController@index')->name('settings');
    Route::post('settings', 'SettingController@updateSettings')->name('update-settings');
    Route::get('profile', 'AdminController@profile')->name('profile');
    Route::post('profile', 'AdminController@updateProfile')->name('update-profile');
     Route::resource('admins', AdminController::class);
    Route::get('users/subscriptions/{id}', [UserController::class, 'subscriptions']);
    Route::get('users/payments/{id}', [UserController::class, 'payments']);
    Route::get('users/send-password/{id}', [UserController::class, 'sendPassword']);
    Route::get('users/cloud-accounts/{id}', [UserController::class, 'cloudAccounts']);
    Route::get('users/cloud-migrations-report/{id}', [UserController::class, 'cloudMigrationsReport']);
    Route::get('users/cloud-migration-report-detail/{id}', [UserController::class , 'cloudMigrationReportDetail']);
    Route::get('users/mailbox-migrations-report/{id}', [UserController::class, 'mailboxMigrationsReport']);
    Route::get('users/packages/{id}', [UserController::class, 'packages']);
    Route::post('users/update-package', [UserController::class, 'updatePackage']);
    Route::resource('users',UserController::class);
    Route::resource('roles',RoleController::class);
    Route::resource('package-features',PackageFeatureController::class);
    Route::resource('package-settings',PackageSettingController::class);
    Route::get('packages/subscriptions/{id}',[PackageController::class, 'subscriptions']);
    Route::get('packages/clone/{id}',[PackageController::class, 'clone']);
    Route::resource('packages',PackageController::class);
    Route::resource('faqs',FaqController::class);
    Route::resource('countries',CountryController::class);
    Route::resource('languages',LanguageController::class);
    Route::resource('label-translations',LabelTranslationController::class);
    Route::resource('subscriptions',SubscriptionController::class);
    Route::resource('cms-pages',CmsPagesController::class);
    Route::resource('cms-page-labels',CmsPageLabelController::class);
    Route::resource('email-templates',EmailTemplateController::class);
    Route::resource('email-template-labels',EmailTemplateLabelController::class);
    Route::resource('contact-us-queries',ContactUsQueryController::class);
    Route::resource('features',FeaturesController::class);
    Route::resource('services',ServicesController::class);
    Route::resource('home-contents',HomeContentController::class);
    Route::resource('home-content-labels', HomeContentLabelController::class);
    Route::get('language-translations/partial-translate', [LanguageTranslationController::class, 'partialTranslate']);
    Route::post('language-translations/partial-translate', [LanguageTranslationController::class, 'addPartialTranslate']);
    Route::resource('language-translations', LanguageTranslationController::class);
    Route::resource('language-modules', LanguageModuleController::class);
    Route::get('payment-gateway-settings',[PaymentGatewaySettingController::class, 'index']);
    Route::post('payment-gateway-settings',[PaymentGatewaySettingController::class, 'update']);
    Route::get('lawful-interception', [LawfulInterceptionController::class, 'index']);
    Route::get('lawful-interception/user-details-pdf/{id}', [LawfulInterceptionController::class, 'userDetailsPdf']);
});


// ******************* //
//     Test Routes
// ******************* //
// Route::get('test', [TestController::class, 'index']);
// Route::get('test/activate-free-package', [TestController::class, 'activateFreePackage']);
// Route::any('test/amazon-pay',[TestController::class, 'amazonPay']);
// Route::get('test/update-users-timezone', [TestController::class, 'updateUsersTimezone']);
// Route::get('test/empty-subscriptions', [TestController::class, 'emptySubscriptions']);
// Route::get('test/apply-prior-payasyougo', [TestController::class, 'applyPriorPayAsYouGoToDifferentTables']);
// Route::get('test/subscribe-payasyougo', [TestController::class, 'subscribePayAsYouGo']);

// yahoo testing
Route::get('contact-auth-url', [OauthController::class, 'googleDriveAuthUrl']);
Route::get('yahoo-callback', [OauthController::class, 'yahooContactsCallback']);

// ******************** //
//     App Routes
// ******************** //

// Route::get('/', function () {
//     return view('app');
// });

// Route::get('/{url}/{slug?}', function ($url) {
//     return view('app');
// })->where('url', '(signup|signin|dashboard|logout|create-migration|profile|settings|forgot-password|contact-us|clouds/add|clouds/callback|clouds/manage|clouds/migrate|clouds/calculate-cloud-source|clouds/migration-reports|clouds/migration-report-detail|mailbox/migrate|pages|resetPassword/find|reset-password|billing|upgrade-package|verify-account|mailbox/migration-reports|payments|payment-checkout|klarna-confirmation|alipay-confirmation|contacts/migrate|about-us|features|page1|page2|page3|page4|contacts/migration-reports|mollie-confirmation|packages|clouds/mail-authentication|contacts/authentication)');

