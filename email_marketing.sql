-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2021 at 03:43 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `email_marketing`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_settings`
--

CREATE TABLE `account_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_holder_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four_digits` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expire_month` tinyint(1) DEFAULT NULL,
  `expire_year` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvc` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `migration_init_notification` tinyint(1) DEFAULT NULL,
  `migration_complete_notification` tinyint(1) DEFAULT NULL,
  `blacklist_email` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_reset_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_reset_token_date` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `role_id`, `name`, `email`, `password`, `original_password`, `profile_image`, `remember_token`, `password_reset_token`, `password_reset_token_date`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Super Admin', 'admin@arhamsoft.com', '$2y$10$6ncnwbG24rQ7FissRfEEZuymJsVc/C8GZUYzbkzdC3epRhdGYIvRS', '12345678', 'profile-image-5ea16b5b33ebf.png', 'KAEbw5YOQyULLDTYoOX5mtJTe5OtaG2M2cqnSsl66oNMXyXrLO6Zix6xAVK5', NULL, NULL, 1, NULL, '2019-09-17 09:47:46', '2020-04-23 00:18:03'),
(2, 2, 'Brett Richard', 'kowoze@mailinator.com', '$2y$10$oqEFhHjMki.7aykSNgGHIOAfYVt4Z0NcSG53.D1iJou6WZPrbv8Bq', '12345678', NULL, NULL, NULL, NULL, 1, NULL, '2020-04-28 19:41:27', '2020-05-18 00:03:10'),
(3, 1, 'Ali Hassan', 'arhamsoft.ali@gmail.com', '$2y$10$jHN37.6KeKOoPhGPZGo.yODZHtt5pVLl1bzSTb9VXLNCaCxYhXx1a', 'User@123', NULL, NULL, NULL, NULL, 1, NULL, '2020-05-18 21:34:14', '2020-05-18 21:34:28'),
(4, 2, 'Bilal Ahmad', 'bilal.ahmed@arhamsoft.com', '$2y$10$XCojC67OQaj61hSgrj9EzeYIJaL43QjuBNfy/U.8NEpmRii2dt952', 'Bil123@@', NULL, NULL, NULL, NULL, 1, NULL, '2020-09-10 19:45:12', '2020-09-10 19:45:12');

-- --------------------------------------------------------

--
-- Table structure for table `clouds`
--

CREATE TABLE `clouds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `cloud_id` tinyint(1) NOT NULL DEFAULT 0,
  `username` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refresh_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires_in` int(11) DEFAULT NULL,
  `timestamp` bigint(20) DEFAULT 0,
  `total_space` bigint(11) DEFAULT 0,
  `used_space` bigint(11) DEFAULT 0,
  `available_space` bigint(11) DEFAULT 0,
  `auth_response` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cloud_migration_reports`
--

CREATE TABLE `cloud_migration_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `package_subscription_id` bigint(20) UNSIGNED DEFAULT NULL,
  `source_cloud_id` bigint(20) UNSIGNED NOT NULL,
  `source_selected_path_ids` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination_cloud_id` bigint(20) UNSIGNED NOT NULL,
  `destination_selected_directory_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_total_files_folders` int(11) DEFAULT 0,
  `no_of_processed_files_folders` int(11) DEFAULT 0,
  `no_of_conflict_files_folders` int(11) DEFAULT 0,
  `notify_me` tinyint(1) DEFAULT NULL,
  `notify_emails` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_payload` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost_payload` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_id` bigint(20) DEFAULT NULL,
  `is_invoice` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=pending,1=created',
  `processed_on` bigint(20) DEFAULT NULL,
  `timestamp` bigint(20) DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cloud_migration_report_files`
--

CREATE TABLE `cloud_migration_report_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `report_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) DEFAULT 0,
  `processed_on` bigint(20) DEFAULT 0,
  `timestamp` bigint(20) DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` blob NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_page_labels`
--

CREATE TABLE `cms_page_labels` (
  `id` int(10) UNSIGNED NOT NULL,
  `cms_page_id` int(10) UNSIGNED DEFAULT NULL,
  `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `first_name` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calling_country_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'for sms',
  `number` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'for sms',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscribed` tinyint(1) NOT NULL DEFAULT 1,
  `unsubscribed_at` datetime DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_groups`
--

CREATE TABLE `contact_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `contact_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_queries`
--

CREATE TABLE `contact_us_queries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat` tinyint(1) DEFAULT 0,
  `apply_default_vat` tinyint(1) DEFAULT 0,
  `status` tinyint(1) DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_campaigns`
--

CREATE TABLE `email_campaigns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sending_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sending_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `html` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `track_opens` tinyint(1) NOT NULL DEFAULT 0,
  `track_clicks` tinyint(1) NOT NULL DEFAULT 0,
  `sent_to_number` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_campaign_click_links`
--

CREATE TABLE `email_campaign_click_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_campaign_links`
--

CREATE TABLE `email_campaign_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_campaign_unsubscribes`
--

CREATE TABLE `email_campaign_unsubscribes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_sending_logs`
--

CREATE TABLE `email_sending_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `sent_at` datetime DEFAULT NULL,
  `opened_at` datetime DEFAULT NULL,
  `bounced_at` datetime DEFAULT NULL,
  `complaint_at` datetime DEFAULT NULL,
  `unsubscribed_at` datetime DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `failed_reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` blob NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_template_labels`
--

CREATE TABLE `email_template_labels` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_template_id` int(10) UNSIGNED DEFAULT NULL,
  `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs--`
--

CREATE TABLE `failed_jobs--` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `group_type` tinyint(4) DEFAULT NULL COMMENT '1=email,2=sms',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `double_opt_in` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_contents`
--

CREATE TABLE `home_contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_content_labels`
--

CREATE TABLE `home_content_labels` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_content_id` int(10) UNSIGNED DEFAULT NULL,
  `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `language_modules`
--

CREATE TABLE `language_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `columns` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `language_translations`
--

CREATE TABLE `language_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language_module_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `language_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `column_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `item_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` tinyint(1) NOT NULL DEFAULT 1,
  `custom` tinyint(1) DEFAULT 0,
  `editor` tinyint(1) DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 2),
(3, '2019_08_19_000000_create_failed_jobs_table', 3),
(4, '2021_08_06_132240_create_permission_tables', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(1) DEFAULT 1,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint(1) DEFAULT 0,
  `link` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fa_class` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets---`
--

CREATE TABLE `password_resets---` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions---`
--

CREATE TABLE `permissions---` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE `rights` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rights`
--

INSERT INTO `rights` (`id`, `module_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Roles Listing', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:19'),
(2, 1, 'Create Role', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(3, 1, 'Edit Role', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(4, 2, 'Sub-Admins Listing', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(5, 2, 'Create Sub-Admin', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(6, 2, 'Edit Sub-Admin', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(7, 2, 'Delete Sub-Admin', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(8, 3, 'Users Listing', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(9, 3, 'Add User', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(10, 3, 'Edit User', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(11, 3, 'Delete User', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(12, 3, 'User Subscriptions', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(13, 4, 'Package Features Listing', 1, '2019-12-01 00:00:00', '2019-09-21 00:00:00'),
(14, 4, 'Add Package Feature', 0, '2019-12-01 00:00:00', '2019-12-01 00:00:00'),
(15, 4, 'Edit Package Feature', 1, '2019-12-01 00:00:00', '2019-12-01 00:00:00'),
(16, 4, 'Delete Package Feature', 0, '2019-12-01 00:00:00', '2019-12-01 00:00:00'),
(17, 5, 'Packages listing', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(18, 5, 'Add Package', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(19, 5, 'Edit Package', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(20, 5, 'Delete Package', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(21, 5, 'Package Subscriptions', 1, '2019-09-28 00:00:00', '2019-09-21 00:00:00'),
(22, 6, 'Subscriptions Listing', 1, '2019-09-28 00:00:00', '2019-09-21 00:00:00'),
(23, 7, 'FAQ Listing', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(24, 7, 'Add FAQ', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(25, 7, 'Edit FAQ', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(26, 7, 'Delete FAQ', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(27, 8, 'Languages Listing', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(28, 8, 'Add Language', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(29, 8, 'Edit Language', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(30, 8, 'Delete Language', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(31, 9, 'Email Templates Listing', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(32, 9, 'Add Email Template', 0, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(33, 9, 'Edit Email Template', 1, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(34, 9, 'Delete Email Template', 0, '2019-12-02 12:20:34', '2019-12-02 12:20:34'),
(35, 2, 'Edit Profile', 1, '2019-09-28 00:00:00', '2019-09-21 00:00:00'),
(36, 2, 'Site Setting', 1, '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
(37, 3, 'User Payments', 1, '2019-12-11 00:00:00', '2019-12-12 00:00:00'),
(38, 10, 'CMS Pages Listing', 1, '2019-09-28 00:00:00', '2019-09-21 00:00:00'),
(39, 10, 'Add CMS Page', 1, '2019-12-30 18:37:34', '2019-12-02 00:00:00'),
(40, 10, 'Edit CMS Page', 1, '2019-09-28 00:00:00', '2019-09-21 00:00:00'),
(41, 10, 'Delete CMS Page', 1, '2019-12-30 18:38:11', '2019-12-02 00:00:00'),
(42, 11, 'View Notifications ', 1, '2019-09-28 00:00:00', '2019-09-21 00:00:00'),
(43, 1, 'Delete Role', 1, '2020-09-11 08:42:17', '2020-09-11 08:42:17'),
(44, 12, 'Features Listing', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(45, 12, 'Add Feature', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(46, 12, 'Edit Feature', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(47, 12, 'Delete Feature', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(48, 13, 'Services Listing', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(49, 13, 'Add Service', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(50, 13, 'Edit Service', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(51, 13, 'Delete Service', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(52, 14, 'Home Contents Listing', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(53, 14, 'Add Home Content', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(54, 14, 'Edit Home Content', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(55, 14, 'Delete Home Content', 1, '2019-11-29 18:42:17', '2019-11-29 18:42:17'),
(56, 15, 'Contact Us Queries Listing', 1, '2019-09-28 00:00:00', '2019-09-28 00:00:00'),
(57, 15, 'Edit Contact Us Query', 1, '2019-09-28 00:00:00', '2019-09-28 00:00:00'),
(58, 15, 'Payment Gateway Settings', 1, '2019-09-28 00:00:00', '2019-09-28 00:00:00'),
(85, 3, 'Update Package', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(86, 20, 'Lawful Interception Listing', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(87, 20, 'Filter', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(88, 20, 'Users Listing', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(89, 20, 'User Details PDF', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(90, 20, 'Caldav / Cardav Migration Reports', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(91, 20, 'Cloud Migration Reports', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(92, 20, 'Mailbox Migration Reports', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(93, 20, 'User Payments PDF', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(94, 20, 'User Subscriptions PDF', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(95, 20, 'Download All Data', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(96, 21, 'Countries Listing', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(97, 21, 'Create Country', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(98, 21, 'Edit Country', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(99, 21, 'Delete Country', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(100, 22, 'Language Modules Listing', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(101, 22, 'Create Language Module', 0, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(102, 22, 'Edit Language Module', 0, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(103, 22, 'Delete Language Module', 0, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(104, 3, 'Download Invoice', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00'),
(105, 5, 'Clone', 1, '2020-12-11 00:00:00', '2020-12-11 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles---`
--

CREATE TABLE `roles---` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `right_ids` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles---`
--

INSERT INTO `roles---` (`id`, `name`, `right_ids`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', NULL, 1, NULL, '2019-09-17 09:47:46', '2019-09-17 14:47:46'),
(2, 'Manager', '1,4,8,18,19,20,21,38,40,42,48', 1, NULL, '2019-09-17 09:51:35', '2020-09-11 07:33:38'),
(3, 'Assistant', NULL, 1, NULL, '2020-04-29 00:42:02', '2020-04-29 00:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `option_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sms_campaigns`
--

CREATE TABLE `sms_campaigns` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `sent_to_number` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sms_sending_logs`
--

CREATE TABLE `sms_sending_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) NOT NULL,
  `campaign_id` bigint(20) NOT NULL,
  `sent_at` datetime DEFAULT NULL,
  `sending_status` int(11) DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `failed_reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api response',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE `timezones` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `utc_offset` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users----`
--

CREATE TABLE `users----` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_settings`
--
ALTER TABLE `account_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `clouds`
--
ALTER TABLE `clouds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cloud_migration_reports`
--
ALTER TABLE `cloud_migration_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `cloud_migration_reports_ibfk_2` (`source_cloud_id`),
  ADD KEY `cloud_migration_reports_ibfk_3` (`destination_cloud_id`),
  ADD KEY `package_subscription_id` (`package_subscription_id`);

--
-- Indexes for table `cloud_migration_report_files`
--
ALTER TABLE `cloud_migration_report_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `report_id` (`report_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_page_labels`
--
ALTER TABLE `cms_page_labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_page_id` (`cms_page_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_groups`
--
ALTER TABLE `contact_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us_queries`
--
ALTER TABLE `contact_us_queries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_campaigns`
--
ALTER TABLE `email_campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_campaign_click_links`
--
ALTER TABLE `email_campaign_click_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_campaign_links`
--
ALTER TABLE `email_campaign_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_campaign_unsubscribes`
--
ALTER TABLE `email_campaign_unsubscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_sending_logs`
--
ALTER TABLE `email_sending_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_template_labels`
--
ALTER TABLE `email_template_labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_template_id` (`email_template_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `failed_jobs--`
--
ALTER TABLE `failed_jobs--`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_contents`
--
ALTER TABLE `home_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_content_labels`
--
ALTER TABLE `home_content_labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `home_content_id` (`home_content_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language_modules`
--
ALTER TABLE `language_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language_translations`
--
ALTER TABLE `language_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `language_module_id` (`language_module_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_resets---`
--
ALTER TABLE `password_resets---`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `permissions---`
--
ALTER TABLE `permissions---`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `roles---`
--
ALTER TABLE `roles---`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_campaigns`
--
ALTER TABLE `sms_campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_sending_logs`
--
ALTER TABLE `sms_sending_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timezones`
--
ALTER TABLE `timezones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users----`
--
ALTER TABLE `users----`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_settings`
--
ALTER TABLE `account_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `clouds`
--
ALTER TABLE `clouds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cloud_migration_reports`
--
ALTER TABLE `cloud_migration_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cloud_migration_report_files`
--
ALTER TABLE `cloud_migration_report_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_page_labels`
--
ALTER TABLE `cms_page_labels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_groups`
--
ALTER TABLE `contact_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us_queries`
--
ALTER TABLE `contact_us_queries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_campaigns`
--
ALTER TABLE `email_campaigns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_campaign_click_links`
--
ALTER TABLE `email_campaign_click_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_campaign_links`
--
ALTER TABLE `email_campaign_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_campaign_unsubscribes`
--
ALTER TABLE `email_campaign_unsubscribes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_sending_logs`
--
ALTER TABLE `email_sending_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_template_labels`
--
ALTER TABLE `email_template_labels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs--`
--
ALTER TABLE `failed_jobs--`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_contents`
--
ALTER TABLE `home_contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_content_labels`
--
ALTER TABLE `home_content_labels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `language_modules`
--
ALTER TABLE `language_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `language_translations`
--
ALTER TABLE `language_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions---`
--
ALTER TABLE `permissions---`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rights`
--
ALTER TABLE `rights`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles---`
--
ALTER TABLE `roles---`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sms_sending_logs`
--
ALTER TABLE `sms_sending_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `timezones`
--
ALTER TABLE `timezones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users----`
--
ALTER TABLE `users----`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_settings`
--
ALTER TABLE `account_settings`
  ADD CONSTRAINT `account_settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users----` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `clouds`
--
ALTER TABLE `clouds`
  ADD CONSTRAINT `clouds_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users----` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cloud_migration_reports`
--
ALTER TABLE `cloud_migration_reports`
  ADD CONSTRAINT `cloud_migration_reports_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users----` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cloud_migration_reports_ibfk_2` FOREIGN KEY (`source_cloud_id`) REFERENCES `clouds` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cloud_migration_reports_ibfk_3` FOREIGN KEY (`destination_cloud_id`) REFERENCES `clouds` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cloud_migration_report_files`
--
ALTER TABLE `cloud_migration_report_files`
  ADD CONSTRAINT `cloud_migration_report_files_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `cloud_migration_reports` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cloud_migration_report_files_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users----` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cms_page_labels`
--
ALTER TABLE `cms_page_labels`
  ADD CONSTRAINT `cms_page_labels_ibfk_1` FOREIGN KEY (`cms_page_id`) REFERENCES `cms_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `email_template_labels`
--
ALTER TABLE `email_template_labels`
  ADD CONSTRAINT `email_template_labels_ibfk_1` FOREIGN KEY (`email_template_id`) REFERENCES `email_templates` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `language_translations`
--
ALTER TABLE `language_translations`
  ADD CONSTRAINT `language_translations_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `language_translations_ibfk_2` FOREIGN KEY (`language_module_id`) REFERENCES `language_modules` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users----` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
