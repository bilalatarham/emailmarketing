<?php
    $segment_2 = Request::segment(2);
    $segment_3 = Request::segment(3);
?>

<div id="sidebar-nav" class="sidebar ad-pannel-sdbar-sty">
    <nav>
        <ul class="nav" id="sidebar-nav-menu">
            <li>
                <a href="{{route('admin.dashboard')}}" class="{{($segment_2 == 'dashboard') ? 'active' : ''}}">
                    <i class="fa fa-pie-chart"></i><span class="title">Dashboard</span>
                </a>
            </li>
            <?php
                if($segment_2 == 'roles' || $segment_2 == 'admins' || $segment_2 == 'users' || $segment_2 == 'lawful-interception')
                {
                    $users_active_class = 'active';
                    $users_aria_expanded = 'true';
                    $users_div_height = '';
                    $users_div_collapse_class = 'collapse in';
                }
                else
                {
                    $users_active_class = 'collapsed';
                    $users_aria_expanded = 'false';
                    $users_div_height = 'height: 0px';
                    $users_div_collapse_class = 'collapse';
                }
            ?>

            @if(have_right(1) || have_right(5) || have_right(11) || have_right(86))
            <li class="panel">
                <a href="#users" data-toggle="collapse" data-parent="#sidebar-nav-menu"
                    class="{{ $users_active_class }} drop-menu-links" aria-expanded="{{ $users_aria_expanded }}">
                    <i class="fa fa-users"></i><span class="title">Users</span>
                </a>

                <div id="users" class="{{ $users_div_collapse_class }}" aria-expanded="{{ $users_aria_expanded }}"
                    style="{{ $users_div_height }}">
                    <ul class="submenu">
                        @if(have_right(1))
                        <li>
                            <a href="{{ url('admin/roles') }}" class="{{($segment_2 == 'roles') ? 'active' : ''}}">
                                <i class="fa fa-user-secret"></i><span class="title">Roles</span>
                            </a>
                        </li>
                        @endif
                        @if(have_right(5))
                        <li>
                            <a href="{{ url('admin/admins') }}" class="{{($segment_2 == 'admins') ? 'active' : ''}}">
                                <i class="fa fa-user"></i> <span class="title">Admin Users</span>
                            </a>
                        </li>
                        @endif
                        @if(have_right(11))
                        <li>
                            <a href="{{ url('admin/users') }}" class="{{($segment_2 == 'users') ? 'active' : ''}}">
                                <i class="fa fa-users"></i><span class="title">Users</span>
                            </a>
                        </li>
                        @endif
                        @if(have_right(86))
                        <li>
                            <a href="{{ url('admin/lawful-interception') }}"
                                class="{{($segment_2 == 'lawful-interception') ? 'active' : ''}}">
                                <i class="fa fa-user-secret"></i><span class="title">Lawful Interception</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </li>
            @endif

            <?php
                if($segment_2 == 'packages' || $segment_2 == 'package-features' || $segment_2 == 'package-settings')
                {
                    $packages_active_class = 'active';
                    $packages_aria_expanded = 'true';
                    $packages_div_height = '';
                    $packages_div_collapse_class = 'collapse in';
                }
                else
                {
                    $packages_active_class = 'collapsed';
                    $packages_aria_expanded = 'false';
                    $packages_div_height = 'height: 0px';
                    $packages_div_collapse_class = 'collapse';
                }
            ?>
            @if(have_right(20) || have_right(24))
            <li class="panel">
                <a href="#packages" data-toggle="collapse" data-parent="#sidebar-nav-menu"
                    class="{{ $packages_active_class }} drop-menu-links" aria-expanded="{{ $packages_aria_expanded }}">
                    <img src="{{ asset('images/package.png') }}" alt="package" class="img-responsive"><span
                        class="title">Packages</span>
                </a>

                <div id="packages" class="{{ $packages_div_collapse_class }}"
                    aria-expanded="{{ $packages_aria_expanded }}" style="{{ $packages_div_height }}">
                    <ul class="submenu">
                        {{-- @if(have_right(20))
                            <li>
                                <a href="{{ url('admin/package-features') }}"
                        class="{{($segment_2 == 'package-features') ? 'active' : ''}}">
                        <i class="fa fa-list"></i><span class="title">Package Features</span>
                        </a>
            </li>
            @endif --}}
            @if(have_right(20))
            <li>
                <a href="{{ url('admin/package-settings') }}"
                    class="{{($segment_2 == 'package-settings') ? 'active' : ''}}">
                    <i class="fa fa-list"></i><span class="title">Package Settings</span>
                </a>
            </li>
            @endif
            @if(have_right(24))
            <li>
                <a href="{{ url('admin/packages') }}" class="{{($segment_2 == 'packages') ? 'active' : ''}}">
                    <img src="{{ asset('images/package.png') }}" alt="package" class="img-responsive simple"> <img
                        src="{{ asset('images/package-blue.png') }}" alt="package" class="img-responsive activ"><span
                        class="title">Packages</span>
                </a>
            </li>
            @endif
        </ul>
</div>
</li>
@endif

@if(have_right(29))
<li>
    <a href="{{ url('admin/faqs') }}" class="{{($segment_2 == 'faqs') ? 'active' : ''}}">
        <i class="fa fa-question"></i><span class="title">FAQs</span>
    </a>
</li>
@endif

@if(have_right(96))
<li>
    <a href="{{ url('admin/countries') }}" class="{{($segment_2 == 'countries') ? 'active' : ''}}">
        <i class="fa fa-globe"></i><span class="title">Country wise VAT</span>
    </a>
</li>
@endif

<?php
                if($segment_2 == 'languages' || $segment_2 == 'language-translations' || $segment_2 == 'label-translations' || $segment_2 == 'language-modules')
                {
                    $languages_active_class = 'active';
                    $languages_aria_expanded = 'true';
                    $languages_div_height = '';
                    $languages_div_collapse_class = 'collapse in';
                }
                else
                {
                    $languages_active_class = 'collapsed';
                    $languages_aria_expanded = 'false';
                    $languages_div_height = 'height: 0px';
                    $languages_div_collapse_class = 'collapse';
                }
            ?>

@if(have_right(33) || have_right(37) || have_right(43) || have_right(100))
<li class="panel">
    <a href="#languages" data-toggle="collapse" data-parent="#sidebar-nav-menu"
        class="{{ $languages_active_class }} drop-menu-links" aria-expanded="{{ $languages_aria_expanded }}">
        <i class="fa fa-language"></i><span class="title">Languages</span>
    </a>

    <div id="languages" class="{{ $languages_div_collapse_class }}" aria-expanded="{{ $languages_aria_expanded }}"
        style="{{ $languages_div_height }}">
        <ul class="submenu">
            @if(have_right(33))
            <li>
                <a href="{{ url('admin/languages') }}" class="{{($segment_2 == 'languages') ? 'active' : ''}}">
                    <i class="fa fa-language"></i><span class="title">Languages</span>
                </a>
            </li>
            @endif
            @if(have_right(37))
            <li>
                <a href="{{ url('admin/language-translations') }}"
                    class="{{($segment_2 == 'language-translations') ? 'active' : ''}}">
                    <i class="fa fa-list"></i><span class="title">Language Translations</span>
                </a>
            </li>
            @endif
            @if(have_right(43))
            <li>
                <a href="{{ url('admin/label-translations') }}"
                    class="{{($segment_2 == 'label-translations') ? 'active' : ''}}">
                    <i class="fa fa-list"></i><span class="title">Label Translations</span>
                </a>
            </li>
            @endif
            @if(have_right(100))
            <li>
                <a href="{{ url('admin/language-modules') }}"
                    class="{{($segment_2 == 'language-modules') ? 'active' : ''}}">
                    <i class="fa fa-list"></i><span class="title">Language Modules</span>
                </a>
            </li>
            @endif
        </ul>
    </div>
</li>
@endif

<?php
                if($segment_2 == 'email-templates' || $segment_2 == 'email-template-labels')
                {
                    $email_templates_active_class = 'active';
                    $email_templates_aria_expanded = 'true';
                    $email_templates_div_height = '';
                    $email_templates_div_collapse_class = 'collapse in';
                }
                else
                {
                    $email_templates_active_class = 'collapsed';
                    $email_templates_aria_expanded = 'false';
                    $email_templates_div_height = 'height: 0px';
                    $email_templates_div_collapse_class = 'collapse';
                }
            ?>

@if(have_right(44) || have_right(48))
<li class="panel">
    <a href="#email-temp" data-toggle="collapse" data-parent="#sidebar-nav-menu"
        class="{{ $email_templates_active_class }} drop-menu-links"
        aria-expanded="{{ $email_templates_aria_expanded }}">
        <i class="fa fa-envelope"></i><span class="title">Email Templates</span>
    </a>

    <div id="email-temp" class="{{ $email_templates_div_collapse_class }}"
        aria-expanded="{{ $email_templates_aria_expanded }}" style="{{ $email_templates_div_height }}">
        <ul class="submenu">
            @if(have_right(44))
            <li>
                <a href="{{  url('admin/email-templates') }}"
                    class="{{($segment_2 == 'email-templates') ? 'active' : ''}}">
                    <i class="fa fa-envelope"></i><span class="title">Email Templates Listing</span>
                </a>
            </li>
            @endif
            @if(have_right(48))
            <li>
                <a href="{{ url('admin/email-template-labels') }}"
                    class="{{($segment_2 == 'email-template-labels') ? 'active' : ''}}">
                    <i class="fa fa-list"></i><span class="title">Email Template Labels</span>
                </a>
            </li>
            @endif

        </ul>
    </div>
</li>
@endif

<?php
                if($segment_2 == 'cms-pages' || $segment_2 == 'cms-page-labels')
                {
                    $cms_page_active_class = 'active';
                    $cms_page_aria_expanded = 'true';
                    $cms_page_div_height = '';
                    $cms_page_div_collapse_class = 'collapse in';
                }
                else
                {
                    $cms_page_active_class = 'collapsed';
                    $cms_page_aria_expanded = 'false';
                    $cms_page_div_height = 'height: 0px';
                    $cms_page_div_collapse_class = 'collapse';
                }
            ?>

@if(have_right(53) || have_right(57))
<li class="panel">
    <a href="#cms-page" data-toggle="collapse" data-parent="#sidebar-nav-menu"
        class="{{ $cms_page_active_class }} drop-menu-links" aria-expanded="{{ $cms_page_aria_expanded }}">
        <i class="fa fa-file-text-o"></i><span class="title">CMS Pages</span>
    </a>

    <div id="cms-page" class="{{ $cms_page_div_collapse_class }}" aria-expanded="{{ $cms_page_aria_expanded }}"
        style="{{ $cms_page_div_height }}">
        <ul class="submenu">
            @if(have_right(53))
            <li>
                <a href="{{ url('admin/cms-pages') }}" class="{{($segment_2 == 'cms-pages') ? 'active' : ''}}">
                    <i class="fa fa-file-text-o"></i><span class="title">CMS Pages Listing</span>
                </a>
            </li>
            @endif
            @if(have_right(57))
            <li>
                <a href="{{ url('admin/cms-page-labels') }}"
                    class="{{($segment_2 == 'cms-page-labels') ? 'active' : ''}}">
                    <i class="fa fa-list"></i><span class="title">CMS Page Labels</span>
                </a>
            </li>
            @endif

        </ul>
    </div>
</li>
@endif

@if(have_right(62))
<li>
    <a href="{{ url('admin/features') }}" class="{{($segment_2 == 'features') ? 'active' : ''}}">
        <i class="fa fa-tags"></i><span class="title">Features</span>
    </a>
</li>
@endif
@if(have_right(66))
<li>
    <a href="{{ url('admin/services') }}" class="{{($segment_2 == 'services') ? 'active' : ''}}">
        <i class="fa fa-server"></i><span class="title">Services</span>
    </a>
</li>
@endif
<?php
                if($segment_2 == 'home-contents' || $segment_2 == 'home-content-labels')
                {
                    $home_contents_active_class = 'active';
                    $home_contents_aria_expanded = 'true';
                    $home_contents_div_height = '';
                    $home_contents_div_collapse_class = 'collapse in';
                }
                else
                {
                    $home_contents_active_class = 'collapsed';
                    $home_contents_aria_expanded = 'false';
                    $home_contents_div_height = 'height: 0px';
                    $home_contents_div_collapse_class = 'collapse';
                }
            ?>

@if(have_right(70) || have_right(74))
<li class="panel">
    <a href="#home-contents" data-toggle="collapse" data-parent="#sidebar-nav-menu"
        class="{{ $home_contents_active_class }} drop-menu-links" aria-expanded="{{ $home_contents_aria_expanded }}">
        <i class="fa fa-file-o"></i><span class="title">Home Contents</span>
    </a>

    <div id="home-contents" class="{{ $home_contents_div_collapse_class }}"
        aria-expanded="{{ $home_contents_aria_expanded }}" style="{{ $home_contents_div_height }}">
        <ul class="submenu">
            @if(have_right(70))
            <li>
                <a href="{{  url('admin/home-contents') }}" class="{{($segment_2 == 'home-contents') ? 'active' : ''}}">
                    <i class="fa fa-file-o"></i><span class="title">Home Contents Listing</span>
                </a>
            </li>
            @endif
            @if(have_right(74))
            <li>
                <a href="{{ url('admin/home-content-labels') }}"
                    class="{{($segment_2 == 'home-content-labels') ? 'active' : ''}}">
                    <i class="fa fa-list"></i><span class="title">Home Content Labels</span>
                </a>
            </li>
            @endif

        </ul>
    </div>
</li>
@endif
@if(have_right(79))
<li>
    <a href="{{ url('admin/contact-us-queries') }}" class="{{($segment_2 == 'contact-us-queries') ? 'active' : ''}}">
        <i class="fa fa-phone"></i><span class="title">Contact Us Queries</span>
    </a>
</li>
@endif
@if(have_right(81))
<li>
    <a href="{{ url('admin/payment-gateway-settings') }}"
        class="{{($segment_2 == 'payment-gateway-settings') ? 'active' : ''}}">
        <i class="fa fa-credit-card-alt"></i><span class="title">Payment Gateways</span>
    </a>
</li>
@endif
@if(have_right(10))
<li>
    <a href="{{ url('admin/settings') }}" class="{{($segment_2 == 'settings') ? 'active' : ''}}">
        <i class="fa fa-cog"></i><span class="title">Site Settings</span>
    </a>
</li>
@endif
</ul>
</nav>
</div>