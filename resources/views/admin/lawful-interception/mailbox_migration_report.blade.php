@extends('admin.lawful-interception.template')

@section('title', 'User Mailbox Migration Report')
@section('content')

<table style="width: 100%;border-collapse: collapse;border-spacing: 0;margin-bottom: 20px;">
    <thead style="border:solid #c7c7c7;border-width:1px 1px 0;">
        <tr>
            <th class="service"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;">
                {{ $lang_arr['template']['attribute'] }}</th>
            <th class="desc"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;">
                {{ $lang_arr['template']['value'] }}</th>
        </tr>
    </thead>
    <tbody style="border:solid #c7c7c7;border-width:1px 0 0 1px;">
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['source_email_id'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$detail->source_email_address}}</td>
        </tr>

        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['source_name'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$detail->source_server_name}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['destination_email_id'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$detail->destination_email_address}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['destination_name'] }} </td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$detail->destination_server_name}}</td>
        </tr>


        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['source_server_type'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$detail->source_server_type}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['destination_server_type'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$detail->destination_server_type}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['migration_status'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{ $report['status'] ? $report['status'] : $report['failure'] }}
            </td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['total_migrated_folders'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report['total_migrate_folder']}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['total_migrated_emails'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report['message_transfer']}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['total_volume_of_data_moved'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report['total_volume_moved']}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['skip_emails'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report['message_skip']}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['initiated_on'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                @if($detail->initiated_at)
                {{\Carbon\Carbon::createFromTimeStamp($detail->initiated_at)->tz(session('timezone'))->format('d M, Y - h:i A')}}
                @endif
            </td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['completed_on'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                @if($detail->processed_on)
                {{\Carbon\Carbon::createFromTimeStamp($detail->processed_on)->tz(session('timezone'))->format('d M, Y - h:i A')}}
                @endif</td>
        </tr>
    </tbody>
</table>

@if(isset($report['folder']))
<table style="width: 100%;border-collapse: collapse;border-spacing: 0;margin-bottom: 20px;">
    <thead style="border:solid #c7c7c7;border-width:1px 1px 0;">
        <tr>
            <th class="service"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['folder_name'] }}</th>
            <th class="desc"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['email_count'] }}</th>
        </tr>
    </thead>
    <tbody style="border:solid #c7c7c7;border-width:1px 0 0 1px;">
        @foreach($report['folder'] as $folders)
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;">
                {{$folders['name']}}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;">
                {{$folders['no_of_messages']}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection