@extends('admin.layouts.app')

@section('title', 'Lawful Interception')
@section('sub-title', 'Users Listing')
@section('content')
<div class="main-content">
	<div class="content-heading clearfix">
		<ul class="breadcrumb">
			<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
			<li>Lawful Interception</li>
		</ul>
	</div>
	<div class="container-fluid">
		@include('admin.messages')

		@if(have_right(87))
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Filter</h3>
			</div>
			<div class="panel-body">
				<form id="lawful-filter-form" class="form-inline filter-form-des" method="GET">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4">
							<div class="form-group">
								<input type="text" name="search" id="search" class="form-control"
									placeholder="Search by Name, Username or Email">
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
							<div class="form-group">
								<div id="reportrange"
									style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
									<i class="fa fa-calendar"></i>&nbsp;
									<span></span> <i class="fa fa-caret-down"></i>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<a href="{{url('admin/lawful-interception')}}">
								<button type="button" class="btn cancel btn-fullrounded">
									<span>Reset</span>
								</button>
							</a>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<button type="submit" class="btn btn-primary btn-fullrounded btn-apply">
								<span>Apply</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endif

		@if(have_right(88))
		<!-- DATATABLE -->
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Users Listing</h3>
			</div>
			<div class="panel-body">
				<table id="lawful-interception-datatable" class="table table-hover " style="width:100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Id</th>
							<th>Name</th>
							<th>Username</th>
							<th>Email</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END DATATABLE -->
		@endif
	</div>
	<div class="container">
		<!-- Modal -->
		<div class="modal fade" id="depictDownloadingData" tabindex="-1" role="dialog" aria-hidden="true"
			data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog modal-sm">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Data Downloading</h4>
					</div>
					<div class="modal-body">
						<p class="archiving-data">User's files are being archived. Please wait...</p>
						<p class="archived-data" style="display: none">User's files have been archived. Click the
							button below to download
						</p>
						<div class="progress archiving-data">
							<div class="progress-bar progress-bar-striped bg-primary active" role="progressbar"
								aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
							</div>
						</div>
					</div>
					<div class="modal-footer archived-data" style="display: none">
						<a class="btn btn-primary" id="download-data-btn">Download</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script>
	$(function()
		{
			var start = moment().subtract(29, 'days');
			var end = moment();
			var startFilterDate = '';
			var endFilterDate = '';
			function cb(start, end) {
				startFilterDate = start.format('YYYY-MM-DD');
				endFilterDate = end.format('YYYY-MM-DD');
				$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			}

			$('#reportrange').daterangepicker({
				startDate: start,
				endDate: end,
				// minDate: moment().subtract(1, 'year'),
				ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			}, cb);

			cb(start, end);

			if($("#lawful-interception-datatable").length)
			{
				$('#lawful-interception-datatable').dataTable(
				{
					stateSave: true,
					pageLength: 50,
					scrollX: true,
					processing: false,
					searching: false,
					language: { "processing": showOverlayLoader()},
					drawCallback : function( ) {
						hideOverlayLoader();
					},
					responsive: true,
					// dom: 'Bfrtip',
					lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
					serverSide: true,
					ajax: {
						url: '/admin/lawful-interception',
						data: function (d) {
							d.search = $('#search').val();
							d.from = startFilterDate;
							d.to = endFilterDate;
						}
					},
					columns: [
						{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
						{data: 'id', name: 'id'},
						{data: 'name', name: 'name'},
						{data: 'username', name: 'username'},
						{data: 'email', name: 'email'},
						{data: 'status', name: 'status'},
						{data: 'action', name: 'action', orderable: false, searchable: false},
					]
				}).on( 'length.dt', function () {
					showOverlayLoader();
				}).on('page.dt', function () {
					showOverlayLoader();
				}).on( 'order.dt', function () {
					showOverlayLoader();
				}).on( 'search.dt', function () {
					showOverlayLoader();
				});

				$('#lawful-filter-form').on('submit', function (e) {
					e.preventDefault();
					showOverlayLoader();
					$('#lawful-interception-datatable').DataTable().draw();
				});

				$('#download-data-btn').on('click', function()
				{
					$('#depictDownloadingData').modal('hide');
					$('#depictDownloadingData .archiving-data').show();	
					$('#depictDownloadingData .archived-data').hide();
				});

				$('body').on('click', '.lawful-download-user-data', function(e){
					e.preventDefault();
					$('#depictDownloadingData').modal('show');
					
					var dataId = $(this).attr('data-id');
					var dataFrom = $(this).attr('data-from');
					var dataTo = $(this).attr('data-to');

					$.ajax({
						type: 'get',
						url: '{{ url("admin/lawful-interception/archive-user-data/") }}' + '/' + dataId + '?from=' + dataFrom + '&to=' + dataTo,
						success: function (res) {
							if(res != null && res != undefined) {
								if(res.status && res.status == 1)
								{
									var userTempFile =  setInterval(function()
									{ 
										$.ajax({
											type: 'get',
											url: '{{url("admin/lawful-interception/check-user-temp-file")}}' + '/'+dataId,
											async: false,
											success: function (res) {
												if(res != null && res != undefined) {
													if(res.status == 1)
													{
														clearInterval(userTempFile);
														$('#download-data-btn').attr("href", '{{ url("admin/lawful-interception/download-all-data") }}' + '/' + dataId);
														$('#depictDownloadingData .archiving-data').hide();	
														$('#depictDownloadingData .archived-data').show();	
													}
												}
											}
										});				 
									}, 5000);	
								}					
							}
							else
							{
								console.log('not done')
							}
						}
					});		
				})
				

				$('body').on('click', '.lawful-user-files-download', function(){
					showOverlayLoader();
					
					var lawfulUserdownloadFileInteval =  setInterval(function(){ 
						$.ajax({
							type: 'get',
							url: '{{url("admin/lawful-interception/get-lawful-user-download-files-session")}}',
							async: false,
							success: function (res) {
								if(res != null && res != undefined && res.status == 2) {
									clearInterval(lawfulUserdownloadFileInteval);
									hideOverlayLoader();
								}
							}
						});				 
					}, 1000); 
				})
			}
		}
	);	
	
</script>
@endsection