@extends('admin.lawful-interception.template')

@section('title', 'User Contacs / Calendar Migration Report')
@section('content')

<table
    style="width: 100%;border-collapse: collapse;border-spacing: 0;margin-bottom: 20px;font-family: {{ $global_font_family }}, sans-serif;">
    <thead style="border:solid #c7c7c7;border-width:1px 1px 0;">
        <tr>
            <th class="service"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['attribute'] }}</th>
            <th class="desc"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['value'] }}</th>
        </tr>
    </thead>
    <tbody style="border:solid #c7c7c7;border-width:1px 0 0 1px;">
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['reports']['source_server'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                @if($report->source_server == config('constants.contacts_platform')['GOOGLE_CONTACTS'])
                Google
                @elseif($report->source_server == config('constants.contacts_platform')['YAHOO_CONTACTS'])
                Yahoo
                @elseif($report->source_server == config('constants.contacts_platform')['OUTLOOK_CONTACTS'])
                Outlook
                @elseif($report->source_server == config('constants.contacts_platform')['AIKQ_CONTACTS'])
                aikQ
                @endif
            </td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['reports']['destination_server'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                @if($report->destination_server == config('constants.contacts_platform')['GOOGLE_CONTACTS'])
                Google
                @elseif($report->destination_server == config('constants.contacts_platform')['YAHOO_CONTACTS'])
                Yahoo
                @elseif($report->destination_server == config('constants.contacts_platform')['OUTLOOK_CONTACTS'])
                Outlook
                @elseif($report->destination_server == config('constants.contacts_platform')['AIKQ_CONTACTS'])
                aikQ
                @endif
            </td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['reports']['source_email'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{ $report->source_email }}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['reports']['destination_email'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{ $report->destination_email }}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['reports']['no_of_processed_calendar'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report->no_of_processed_calendar}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['reports']['no_of_processed_contacts'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report->no_of_processed_contacts}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['created_on'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{\Carbon\Carbon::parse($report->created_at)->tz(session('timezone'))->format('d M, Y - h:i A')}}
            </td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['processed_on'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{\Carbon\Carbon::createFromTimeStamp($report->processed_on)->tz(session('timezone'))->format('d M, Y - h:i A')}}
            </td>
        </tr>
    </tbody>
</table>

@endsection