@extends('admin.lawful-interception.template')

@section('title', 'User Cloud Migration Report')
@section('content')

<table
    style="width: 100%;border-collapse: collapse;border-spacing: 0;margin-bottom: 20px;font-family: {{ $global_font_family }}, sans-serif;">
    <thead style="border:solid #c7c7c7;border-width:1px 1px 0;">
        <tr>
            <th class="service"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['attribute'] }}</th>
            <th class="desc"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['value'] }}</th>
        </tr>
    </thead>
    <tbody style="border:solid #c7c7c7;border-width:1px 0 0 1px;">
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['source_email_id'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report->sourceCloud->email ? $report->sourceCloud->email : $report->sourceCloud->username}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['destination_email_id'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report->destinationCloud->email ? $report->destinationCloud->email : $report->destinationCloud->username}}
            </td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['source_cloud'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{getCloudMetaData($report->sourceCloud->cloud_id)['name']}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['destination_cloud'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{getCloudMetaData($report->destinationCloud->cloud_id)['name']}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['total_no_file_folder'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report->no_of_total_files_folders}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['total_file_folder_moved'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report->no_of_processed_files_folders}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['skip_existing_file_overwritten'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{$report->no_of_conflict_files_folders}}</td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['created_on'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{\Carbon\Carbon::createFromTimeStamp($report->timestamp)->tz(session('timezone'))->format('d M, Y - h:i A')}}
            </td>
        </tr>
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['processed_on'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 0;vertical-align:top;">
                {{\Carbon\Carbon::createFromTimeStamp($report->processed_on)->tz(session('timezone'))->format('d M, Y - h:i A')}}
            </td>
        </tr>
    </tbody>
</table>

<table style="width: 100%;border-collapse: collapse;border-spacing: 0;margin-bottom: 20px;">
    <thead style="border:solid #c7c7c7;border-width:1px 1px 0;">
        <tr>
            <th class="service"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['name'] }}</th>
            <th class="desc"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['type'] }}</th>
            <th class="desc"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['size'] }}</th>
            <th class="desc"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['status'] }}</th>
            <th class="desc"
                style="text-align:left;padding:8px 10px;font-weight:bold;color:#fff;background:#007dbd;font-weight:normal;font-family: {{ $global_font_family }}, sans-serif;">
                {{ $lang_arr['template']['processed_on'] }}</th>
        </tr>
    </thead>
    <tbody style="border:solid #c7c7c7;border-width:1px 0 0 1px;">
        @foreach($files as $file)
        <tr>
            <td class="service"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;">
                {{$file->name}}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;">
                {{$file->type}}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;">
                {{$file->size > 0 ? formatBytes($file->size) : '-'}}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;">
                {{ $lang_arr['template']['processed'] }}</td>
            <td class="desc"
                style="font-size:14px;text-align:left;padding:8px 10px;border:solid #c7c7c7;border-width:0 1px 1px 1px;vertical-align:top;">
                {{\Carbon\Carbon::createFromTimeStamp($file->processed_on)->tz(session('timezone'))->format('d M, Y - h:i A')}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection