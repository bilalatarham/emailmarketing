@extends('admin.layouts.app')

@section('title', 'Users')
@section('sub-title', 'Packages')
@section('content')
<div class="main-content">
    <div class="content-heading clearfix">

        <ul class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{url('admin/users')}}"><i class="fa fa-user"></i>Users</a></li>
            <li>Packages</li>
        </ul>
    </div>
    <div class="container-fluid">
        @include('admin.messages')
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Packages</h3>
                <div class="right">
                    <span class="label label-default" style="font-size: 90%;">{{$user->name.' - '.$user->email}}</span>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    @foreach ($packages as $package)
                    <form class="form-horizontal label-left" action="{{url('admin/users/update-package')}}"
                        enctype="multipart/form-data" method="POST">
                        @csrf

                        <input name="user_id" type="hidden" value="{{ $user->id }}" />
                        <input name="package_id" type="hidden" value="{{ $package->id }}" />
                        <input id="current_subscription_type" type="hidden" value="{{ $user->subscription->type }}" />

                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail package-thumbnail">
                                <div class="top-head">
                                    <center>
                                        <h3>{{$package->title}}</h3>
                                    </center>
                                </div>
                                <div class="caption package-single">
                                    <!-- <div class="top-head">
											<center><h3>{{$package->title}}</h3></center>
										</div> -->
                                    <!-- <hr> -->
                                    <b>
                                        <span class="pull-left">
                                            {{ config('constants.currency')['symbol'].''. $package->per_unit_price }} /
                                            Migration
                                        </span>
                                        <span class="pull-right">
                                            Total Price:
                                            {{ config('constants.currency')['symbol'].''. $package->total_price }}
                                        </span>
                                    </b>
                                    <br>
                                    <hr>
                                    {!! $package->description !!}
                                    <p
                                        style="pointer-events: {{$package->id == 2 ? 'none' : 'auto'}};opacity: {{$package->id == 2 ? 0 : 1}}">
                                        <label for="payment_option" class="control-label">Select Payment</label>
                                        <select class="form-control" name="payment_option">
                                            <option value="1" selected>
                                                Free
                                            </option>
                                            <option value="2">
                                                Paid
                                            </option>
                                        </select>
                                    </p>
                                    <center>
                                        @if($package->id == $user->package_id)
                                        <button id="current_btn_{{$package->id}}" class="btn btn-primary" type="submit"
                                            {{ ($user->is_expired == 0) ? "disabled" : "" }}>{{ ($user->is_expired == 0) ? "Current" : "Renew" }}</button>
                                        @elseif($package->id > $user->package_id)
                                        <button class="btn btn-primary" type="submit">Upgrade</button>
                                        @else
                                        <button class="btn btn-primary" type="submit">Downgrade</button>
                                        @endif
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    function updateButton(type, current_subscription_type, is_current_package, package_id)
	{
		console.log(type,is_current_package,current_subscription_type,package_id);
		if(is_current_package == 1)
		{
			if(type != current_subscription_type)
			{
				$('#current_btn_'+package_id).removeAttr('disabled');
				$('#current_btn_'+package_id).text('Update');
			}
			else
			{
				$('#current_btn_'+package_id).prop("disabled", true);
				$('#current_btn_'+package_id).text('Current');
			}
		}
	}
</script>
@endsection