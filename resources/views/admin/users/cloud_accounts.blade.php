@extends('admin.layouts.app')

@section('title', 'Users')
@section('sub-title', 'Cloud Accounts')
@section('content')
<div class="main-content">
	<div class="content-heading clearfix">

		<ul class="breadcrumb">
			<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{url('admin/users')}}"><i class="fa fa-user"></i>Users</a></li>
			<li>Cloud Accounts</li>
		</ul>
	</div>
	<div class="container-fluid">

		@include('admin.messages')
		<!-- DATATABLE -->
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Cloud Accounts</h3>
				<div class="right">
					<span class="label label-default" style="font-size: 90%;">{{$user->name.' - '.$user->email}}</span>
				</div>
			</div>
			<div class="panel-body">
				<table id="cloud-accounts-datatable" class="table table-hover scroll-handler" style="width:100%">
					<thead>
						<tr>
							<th>Id</th>
							<th>Cloud</th>
							<th>Name</th>
							<th>Email</th>
							<th>Fullname</th>
							<th>Total Space</th>
							<th>Used Space</th>
							<th>Available Space</th>
							<th>Created At</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END DATATABLE -->
	</div>
</div>
@endsection

@section('js')
<script>
	$(function()
    {
		$('#cloud-accounts-datatable').dataTable(
		{
			pageLength: 50,
			scrollX: true,
			processing: false,
			language: { "processing": showOverlayLoader()},
			drawCallback : function( ) {
		        hideOverlayLoader();
		    },
			responsive: true,
			// dom: 'Bfrtip',
			lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
			serverSide: true,
			ajax: "{{url('admin/users/cloud-accounts/'.$id)}}",
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'cloud_image', name: 'cloud_image'},
				{data: 'cloud_name', name: 'cloud_name'},
				{data: 'email', name: 'email'},
				{data: 'name', name: 'name'},
				{data: 'total_space', name: 'total_space'},
				{data: 'used_space', name: 'used_space'},
				{data: 'available_space', name: 'available_space'},
				{data: 'created_at', name: 'created_at'}
			]
		}).on( 'length.dt', function () {
			showOverlayLoader();
		}).on('page.dt', function () {
	        showOverlayLoader();
	    }).on( 'order.dt', function () {
		    showOverlayLoader();
		}).on( 'search.dt', function () {
    		showOverlayLoader();
		});
	});
</script>
@endsection