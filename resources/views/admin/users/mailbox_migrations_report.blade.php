@extends('admin.layouts.app')

@section('title', 'Users')
@section('sub-title', 'Mailbox Migrations Report')
@section('content')
<div class="main-content">
	<div class="content-heading clearfix">

		<ul class="breadcrumb">
			<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{url('admin/users')}}"><i class="fa fa-user"></i>Users</a></li>
			<li>Mailbox Migrations Report</li>
		</ul>
	</div>
	<div class="container-fluid">

		@include('admin.messages')
		<!-- DATATABLE -->
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Mailbox Migrations Report</h3>
				<div class="right">
					<span class="label label-default" style="font-size: 90%;">{{$user->name.' - '.$user->email}}</span>
				</div>
			</div>
			<div class="panel-body">
				<table id="mailbox-migrations-report-datatable" class="table table-hover scroll-handler"
					style="width:100%">
					<thead>
						<tr>
							<th>Id</th>
							<th>Source Server</th>
							<th>Source Url</th>
							<th>Source Email</th>
							<th>Destination Server</th>
							<th>Destination Url</th>
							<th>Destination Email</th>
							<th>Status</th>
							<th>Processed At</th>
							<th>Created At</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END DATATABLE -->
	</div>
</div>
@endsection

@section('js')
<script>
	$(function()
    {
		$('#mailbox-migrations-report-datatable').dataTable(
		{
			pageLength: 50,
			scrollX: true,
			processing: false,
			language: { "processing": showOverlayLoader()},
			drawCallback : function( ) {
		        hideOverlayLoader();
		    },
			responsive: true,
			// dom: 'Bfrtip',
			lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
			serverSide: true,
			ajax: "{{url('admin/users/mailbox-migrations-report/'.$id)}}",
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'source_server_type', name: 'source_server_type'},
				{data: 'source_server_url', name: 'source_server_url'},
				{data: 'source_email_address', name: 'source_email_address'},
				{data: 'destination_server_type', name: 'destination_server_type'},
				{data: 'destination_server_url', name: 'destination_server_url'},
				{data: 'destination_email_address', name: 'destination_email_address'},
				{data: 'status', name: 'status', orderable: false, searchable: false},
				{data: 'processed_on', name: 'processed_on'},
				{data: 'created_at', name: 'created_at'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		}).on( 'length.dt', function () {
			showOverlayLoader();
		}).on('page.dt', function () {
	        showOverlayLoader();
	    }).on( 'order.dt', function () {
		    showOverlayLoader();
		}).on( 'search.dt', function () {
    		showOverlayLoader();
		});
	});
</script>
@endsection