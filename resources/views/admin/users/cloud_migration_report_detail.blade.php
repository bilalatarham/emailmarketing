@extends('admin.layouts.app')

@section('title', 'Users')
@section('sub-title', 'Cloud Migration Report Detail')
@section('content')
<div class="main-content">
	<div class="content-heading clearfix">

		<ul class="breadcrumb">
			<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{url('admin/users')}}"><i class="fa fa-user"></i>Users</a></li>
			<li><a href="{{url('admin/users/cloud-migrations-report/'.Hashids::encode($report->user_id))}}"><i
						class="fa fa-file"></i>Cloud Migrations Report</a></li>
			<li>Cloud Migration Report Detail</li>
		</ul>
	</div>
	<div class="container-fluid">

		@include('admin.messages')
		<!-- DATATABLE -->
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Cloud Migration Report Detail</h3>
				<div class="right">
					<span class="label label-default" style="font-size: 90%;">No. of files/folders:
						<b>{{$report->no_of_total_files_folders}}</b>, Processed files/folders:
						<b>{{$report->no_of_processed_files_folders}}</b>, Conflict files/folders:
						<b>{{$report->no_of_conflict_files_folders}}</b></span>
				</div>
			</div>
			<div class="panel-body">
				<table id="cloud-migrations-report-detail-datatable" class="table table-hover" style="width:100%">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Type</th>
							<th>Size</th>
							<th>Status</th>
							<th>Processed At</th>
							<th>Created At</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END DATATABLE -->
	</div>
</div>
@endsection

@section('js')
<script>
	$(function()
    {
		$('#cloud-migrations-report-detail-datatable').dataTable(
		{
			pageLength: 50,
			scrollX: true,
			processing: false,
			language: { "processing": showOverlayLoader()},
			drawCallback : function( ) {
		        hideOverlayLoader();
		    },
			responsive: true,
			// dom: 'Bfrtip',
			lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
			serverSide: true,
			ajax: "{{url('admin/users/cloud-migration-report-detail/'.$id)}}",
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'name', name: 'name'},
				{data: 'type', name: 'type'},
				{data: 'size', name: 'size'},
				{data: 'status', name: 'status', orderable: false, searchable: false},
				{data: 'processed_on', name: 'processed_on'},
				{data: 'created_at', name: 'created_at'},
			]
		}).on( 'length.dt', function () {
			showOverlayLoader();
		}).on('page.dt', function () {
	        showOverlayLoader();
	    }).on( 'order.dt', function () {
		    showOverlayLoader();
		}).on( 'search.dt', function () {
    		showOverlayLoader();
		});
	});
</script>
@endsection