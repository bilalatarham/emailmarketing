@extends('admin.layouts.app')
@section('title', 'Users')
@section('sub-title', 'Listing')
@section('content')
<div class="main-content">
	<div class="content-heading clearfix">

		<ul class="breadcrumb">
			<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
			<li>Users</li>
		</ul>
	</div>
	<div class="container-fluid">
		@include('admin.messages')

		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Advance Filters</h3>
			</div>
			<div class="panel-body">
				<form id="users-filter-form" class="form-inline filter-form-des user-form" method="GET">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-12">
							<div class="form-group">
								<select class="form-control" id="package_id">
									<option value="">Select Package</option>
									@foreach ($packages as $package)
									<option value="{{$package->id}}">{{$package->title}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="form-group">
								<select class="form-control" id="platform">
									<option value="">Select Registered Platform</option>
									<option value="1">Web</option>
									<option value="2">Mobile</option>
									<option value="3">Thunderbird</option>
									<option value="4">Outlook</option>
									<option value="5">Transfer Immunity</option>
									<option value="6">Ned Link</option>
									<option value="7">aikQ</option>
									<option value="8">Inbox</option>
									<option value="9">Overmail</option>
									<option value="10">Maili</option>
									<option value="11">Product Immunity</option>
									<option value="12">QR Code</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12">
							<div class="form-group">
								<select class="form-control" id="status">
									<option value="">Select Status</option>
									<option value="0">Disable</option>
									<option value="1">Active</option>
									<option value="2">Unverified</option>
									<option value="3">Deleted</option>
								</select>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12">
							<a href="{{url('admin/users')}}">
								<button type="button" class="btn cancel btn-fullrounded">
									<span>Reset</span>
								</button>
							</a>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12">
							<button type="submit" class="btn btn-primary btn-fullrounded btn-apply">
								<span>Apply</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<!-- DATATABLE -->
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Users Listing</h3>
				@if(have_right(12))
				<div class="right">
					<a href="{{url('admin/users/create')}}" class="pull-right">
						<button title="Add" type="button" class="btn btn-primary btn-lg btn-fullrounded">
							<span>Add</span>
						</button>
					</a>
				</div>
				@endif
			</div>
			<div class="panel-body">
				<table id="users-datatable" class="table table-hover scroll-handler" style="width:100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Id</th>
							<th>Name</th>
							<th>Username</th>
							<th>Email</th>
							<th>Package</th>
							<th>Registered Platform</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END DATATABLE -->
	</div>
</div>
@endsection
@section('js')
<script>
	$(function()
    {
		var package_id = "{{ $package_id }}";

		if(package_id != '')
        {
            $('#package_id').val(package_id);
        }
		
		$('#users-datatable').dataTable(
		{
			pageLength: 50,
			scrollX: true,
			processing: false,
			language: { "processing": showOverlayLoader()},
			drawCallback : function( ) {
		        hideOverlayLoader();
		    },
			responsive: true,
			// dom: 'Bfrtip',
			lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
			serverSide: true,
			ajax: {
				url: '/admin/users',
				data: function (d) {
					d.package_id = $('#package_id').val();
					d.platform = $('#platform').val();
					d.status = $('#status').val();
				}
			},
			columns: [
				{data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'id', name: 'id'},
				{data: 'name', name: 'name'},
				{data: 'username', name: 'username'},
				{data: 'email', name: 'email'},
				{data: 'package_title', name: 'package_title'},
				{data: 'platform', name: 'platform'},
				{data: 'status', name: 'status'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
			]
		}).on( 'length.dt', function () {
			showOverlayLoader();
		}).on('page.dt', function () {
	        showOverlayLoader();
	    }).on( 'order.dt', function () {
		    showOverlayLoader();
		}).on( 'search.dt', function () {
    		showOverlayLoader();
		});
		
        $('#users-filter-form').on('submit', function (e) {
            e.preventDefault();
            showOverlayLoader();
            $('#users-datatable').DataTable().draw();
        });
	});
</script>
@endsection